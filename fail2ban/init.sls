{% set fail2ban = pillar.get('fail2ban', None) %}
{% if fail2ban %}
fail2ban:
  pkg:
    - installed

/etc/fail2ban/filter.d/nginx-4xx.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://fail2ban/filter.d/nginx-4xx.conf
    - require:
      - pkg: fail2ban

/etc/fail2ban/filter.d/nginx-wp-login.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://fail2ban/filter.d/nginx-wp-login.conf
    - require:
      - pkg: fail2ban

/etc/fail2ban/filter.d/nginx-wp-xmlrpc.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://fail2ban/filter.d/nginx-wp-xmlrpc.conf
    - require:
      - pkg: fail2ban

/etc/fail2ban/jail.d/nginx-wp.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://fail2ban/jail.d/nginx-wp.conf
    - require:
      - pkg: fail2ban

# /etc/fail2ban/jail.d/nginx-common.conf:
#   file.managed:
#     - user: root
#     - group: root
#     - mode: 644
#     - source: salt://fail2ban/jail.d/nginx-common.conf
#     - require:
#       - pkg: fail2ban

/etc/fail2ban/jail.d/sshd.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://fail2ban/jail.d/sshd.conf
    - require:
      - pkg: fail2ban

/home/web/opt/f2b-list:
  file.managed:
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - source: salt://fail2ban/f2b-list

{% for user in pillar.get('users', {}) %}
/home/{{ user }}/bin/f2b-list:
  file.symlink:
    - target: /home/web/opt/f2b-list
    - require:
      - file: /home/web/opt/f2b-list
      - user: {{ user }}
      - group: {{ user }}
{% endfor %}
{% endif %}
