{% set docker = pillar.get('docker', None) %}
{% if docker %}

docker.io:
  pkg.installed

{% endif %}
