{% set chat = pillar.get('chat', False) %}
{% set devpi = pillar.get('devpi', None) %}
{% set django = pillar.get('django', None) %}
{% set dropbox = pillar.get('dropbox', None) %}
{% set postgres_settings = pillar.get('postgres_settings') -%}
{% set sites = pillar.get('sites', {}) %}

{% if chat or django or devpi or dropbox %}

{% if django %}
uwsgi:
  supervisord:
    - running
    - restart: True
    - require:
      - pkg: supervisor
{% endif %}

supervisor:
  pkg:
    - installed
  service:
    - running
    - watch:
      {% if chat %}
      - file: /etc/supervisor/conf.d/chat.conf
      {% endif %}
      {% if devpi %}
      - file: /etc/supervisor/conf.d/devpi.conf
      {% endif %}
      {% if django %}
      - file: /etc/supervisor/conf.d/uwsgi.conf
      {% endif %}
      {% if dropbox %}
      {% for account in dropbox.accounts %}
      - file: /etc/supervisor/conf.d/dropbox_{{ account }}.conf
      {% endfor %}
      {% endif %}
      {% for domain, settings in sites.items() %}
      {% if settings.get('dramatiq', None) %}
      - file: /etc/supervisor/conf.d/{{ domain }}-dramatiq.conf
      {% endif %} # dramatiq
      {% if settings.get('scheduler', None) %}
      - file: /etc/supervisor/conf.d/{{ domain }}-scheduler.conf
      {% endif %} # scheduler
      {% endfor %} # site, settings

{% endif %}

{% if devpi %}
/etc/supervisor/conf.d/devpi.conf:
  file:
    - managed
    - source: salt://supervisor/devpi.conf
    - template: jinja
    - context:
      devpi: {{ devpi }}
    - require:
      - pkg: supervisor
{% endif %}

{% if dropbox %}
{% for account in dropbox.accounts %}
/etc/supervisor/conf.d/dropbox_{{ account }}.conf:
  file:
    - managed
    - source: salt://supervisor/dropbox.conf
    - template: jinja
    - context:
      account: {{ account }}
    - require:
      - pkg: supervisor
{% endfor %}
{% endif %}

{% if chat or django %}

{% if django %}

/etc/supervisor/conf.d/uwsgi.conf:
  file:
    - managed
    - source: salt://supervisor/uwsgi.conf
    - require:
      - pkg: supervisor

{% set postgres_settings = pillar.get('postgres_settings') -%}
{% endif %}

{% for domain, settings in sites.items() %}

{% set profile = settings.get('profile') -%}
{% if profile == 'mattermost' %}
/etc/supervisor/conf.d/chat.conf:
  file:
    - managed
    - source: salt://supervisor/chat.conf
    - template: jinja
    - context:
      domain: {{ domain }}
    - require:
      - pkg: supervisor
{% endif %}

{% if settings.get('ftp', None) %}
{% set env = settings['env'] -%}
/etc/supervisor/conf.d/{{ env['ftp_user_name'] }}_watch_ftp_folder.conf:
  file:
    - managed
    - source: salt://supervisor/watch_ftp_folder.conf
    - template: jinja
    - context:
      domain: {{ domain }}
      settings: {{ settings }}
    - require:
      - pkg: supervisor
{% endif %} # ftp
{% if settings.get('dramatiq', None) %}
/etc/supervisor/conf.d/{{ domain }}-dramatiq.conf:
  file:
    - managed
    - source: salt://supervisor/dramatiq.conf
    - template: jinja
    - context:
      domain: {{ domain }}
      postgres_settings: {{ postgres_settings }}
      settings: {{ settings }}
    - require:
      - pkg: supervisor
{% else %}
/etc/supervisor/conf.d/{{ domain }}-dramatiq.conf:
  file.absent
{% endif %} # dramatiq
{% if settings.get('scheduler', None) %}
/etc/supervisor/conf.d/{{ domain }}-scheduler.conf:
  file:
    - managed
    - source: salt://supervisor/scheduler.conf
    - template: jinja
    - context:
      domain: {{ domain }}
      postgres_settings: {{ postgres_settings }}
      settings: {{ settings }}
    - require:
      - pkg: supervisor
{% else %}
/etc/supervisor/conf.d/{{ domain }}-scheduler.conf:
  file.absent
{% endif %} # scheduler
{% endfor %} # site, settings

{% endif %}
