{% set private_network = pillar.get('private_network', None) %}
{% if not private_network %}

{% set server_meta = pillar.get('server_meta', None) %}
{% if server_meta %}
{% set sshd_interface_ips = server_meta.get('sshd_interface_ips', []) %}
{% else %}
{% set sshd_interface_ips = [] %}
{% endif %}

{% if grains['osmajorrelease'] < 20 -%}
ssh:
  service:
    - running
    - watch:
      - file: /etc/ssh/sshd_config

/etc/ssh/sshd_config:
  file:
    - managed
    - source: salt://ssh/sshd_config
    - user: root
    - group: root
    - mode: 644
{% else %}
ssh:
  service:
    - running
    - watch:
      - file: /etc/ssh/sshd_config.d/clientalive.conf

/etc/ssh/sshd_config.d/clientalive.conf:
  file:
    - managed
    - source: salt://ssh/sshd_clientalive.conf
    - user: root
    - group: root
    - mode: 644

/etc/ssh/sshd_config.d/auth.conf:
  file:
    - managed
    - source: salt://ssh/sshd_auth.conf
    - user: root
    - group: root
    - mode: 644

{% if sshd_interface_ips|length > 0 %}
/etc/ssh/sshd_config.d/interface.conf:
  file:
    - managed
    - source: salt://ssh/sshd_interface.conf
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - content:
      sshd_interface_ips: {{ sshd_interface_ips }}

/etc/systemd/system/sshd.service.d/wireguard.conf:
  file:
    - managed
    - source: salt://ssh/wireguard.conf
    - user: root
    - group: root
    - mode: 644
    - makedirs: True

{% else %} {# sshd_interface_ips #}

/etc/ssh/sshd_config.d/interface.conf:
  file.absent

/etc/systemd/system/sshd.service.d/wireguard.conf:
  file.absent

{% endif %} {# sshd_interface_ips #}
{% endif %} {# grains['osmajorrelease'] < 20 #}
{% endif %} {# private_network #}
