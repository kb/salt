{% set development = pillar.get('development', None) -%}
{% if development -%}

python3-dev:
  pkg.installed

{% endif %}
