{% set apache = pillar.get('apache', None) %}
{% if apache %}

{# copied from https://github.com/saltstack-formulas/apache-formula #}

apache:
  pkg:
    - installed
    - name: apache2
  service:
    - running
    - name: apache2
    - enable: True

{% set apache_php = pillar.get('apache_php', None) %}
{% if apache_php %}

php:
  pkg:
    - installed

php-curl:
  pkg:
    - installed
    - require:
      - pkg: php

php-xml:
  pkg:
    - installed
    - require:
      - pkg: php

php-gd:
  pkg:
    - installed
    - require:
      - pkg: php

libapache2-mod-php:
  pkg:
    - installed
    - require:
      - pkg: php
  service:
    - running
    - name: apache2
    - enable: True

libapache2-mod-evasive:
  pkg:
    - installed
    - require:
      - pkg: apache2
  service:
    - running
    - name: apache2
    - enable: True

{% endif %}
{% endif %}
