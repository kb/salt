{# Only set-up uwsgi if we are using Django (including Graphite) #}
{% set django = pillar.get('django', None) %}
{% set sites = pillar.get('sites', {}) %}

{% if django %}

{% set postgres_settings = pillar.get('postgres_settings') -%}

uwsgi-core:
  pkg.installed

uwsgi-plugin-python3:
  pkg.installed

/home/web/repo/uwsgi:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - require:
      - file: /home/web/repo

/home/web/repo/uwsgi/log:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - require:
      - file: /home/web/repo/uwsgi

/home/web/repo/uwsgi/vassals:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - require:
      - file: /home/web/repo/uwsgi

{% for domain, settings in sites.items() %}
{% set profile = settings.get('profile') -%}

{% if profile == 'django' %}

/home/web/repo/uwsgi/vassals/{{ domain }}.ini:
  file:
    - managed
    - source: salt://uwsgi/vassal.ini
    - user: web
    - group: web
    - template: jinja
    - context:
      domain: {{ domain }}
      postgres_settings: {{ postgres_settings }}
      settings: {{ settings }}
    - require:
      - file: /home/web/repo/uwsgi/vassals

{% if settings.get('celery', None) %}
/home/web/repo/uwsgi/vassals/{{ domain }}-celery-beat.ini:
  file:
    - managed
    - source: salt://uwsgi/vassal_celery_beat.ini
    - user: web
    - group: web
    - template: jinja
    - context:
      domain: {{ domain }}
      postgres_settings: {{ postgres_settings }}
      settings: {{ settings }}
    - require:
      - file: /home/web/repo/uwsgi/vassals

/home/web/repo/uwsgi/vassals/{{ domain }}-celery-worker.ini:
  file:
    - managed
    - source: salt://uwsgi/vassal_celery_worker.ini
    - user: web
    - group: web
    - template: jinja
    - context:
      domain: {{ domain }}
      postgres_settings: {{ postgres_settings }}
      settings: {{ settings }}
    - require:
      - file: /home/web/repo/uwsgi/vassals
{% else %} # celery
/home/web/repo/uwsgi/vassals/{{ domain }}-celery-beat.ini:
  file:
    - absent
/home/web/repo/uwsgi/vassals/{{ domain }}-celery-worker.ini:
  file:
    - absent
{% endif %} # celery
{% endif %} # django (profile)
{% endfor %} # domain, settings
{% endif %} # django
