{% set keycloak = pillar.get('keycloak', None) %}

{% if keycloak %}
  {% set sites = pillar.get('sites', {}) -%}
  {% for domain, settings in sites.items() %}
    {% set profile = settings.get('profile') -%}
    {% if profile == 'keycloak' %}

/home/web/repo/project/{{ domain }}/live:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: True

/home/web/repo/project/{{ domain }}/live/modules/system/layers/keycloak/org/postgresql/main/module.xml:
  file:
    - managed
    - user: web
    - group: web
    - source: salt://keycloak/module.xml
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/repo/project/{{ domain }}/live

/home/web/repo/project/{{ domain }}/live/modules/system/layers/keycloak/org/postgresql/main/postgresql-42.2.9.jar:
  file:
    - managed
    - user: web
    - group: web
    - source: salt://workflow/postgresql-42.2.9.jar
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/repo/project/{{ domain }}/live

/home/web/repo/project/{{ domain }}/live/standalone/configuration/standalone.xml:
  file:
    - managed
    - user: web
    - group: web
    - source: salt://keycloak/standalone.xml
    - mode: 755
    - makedirs: True
    - template: jinja
    - context:
      domain: {{ domain }}
      settings: {{ settings }}
    - require:
      - file: /home/web/repo/project/{{ domain }}/live

    {% endif %} # keycloak
  {% endfor %} # domain, settings
{% endif %} # keycloak
