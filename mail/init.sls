{% set server_meta = pillar.get('server_meta', {}) -%} {# from top level secion called server_meta in 'sites/<server profile>.sls' -#}

{% if server_meta %}
{% set mail_server = server_meta.get('mail_server', None) -%}
{% if mail_server %}
{% set local_host = grains['nodename'] %}
{% set users = pillar.get('users', {}) %}
{% set mail_domain = mail_server.get('mail_domain', local_host) %}
{% set admin_email = mail_server.get('admin_email', None) %}
{% set sender_rules = mail_server.get('sender_rules', []) %}

postfix:
  pkg:
    - installed
    - require:
      - pkg: mailutils
  service:
    - running
    - watch:
      - file: /etc/postfix/main.cf
      - file: /etc/mailname

mailutils:
  pkg:
    - installed

/etc/postfix/main.cf:
  file:
    - managed
    - source: salt://mail/postfix/main_cf.conf
    - user: root
    - group: mail
    - mode: 644
    - template: jinja
    - content:
      mail_server: {{ mail_server }}
      server_meta: {{ server_meta }}
      local_host: {{ local_host }}
    - require:
      - pkg: postfix

/etc/mailname:
  file:
    - managed
    - source: salt://mail/postfix/one_line.conf
    - template: jinja
    - context:
      line_contents: {{ mail_domain }}
    - require:
      - pkg: postfix


/etc/aliases:
  file:
    - managed
    - source: salt://mail/postfix/aliases.conf
    - user: root
    - group: mail
    - mode: 644
    - template: jinja
    - context:
      admin_email: {{ admin_email }}
      users: {{ users }}
    - require:
      - pkg: postfix

update_aliases:
  cmd.run:
    - name: /usr/bin/newaliases
    - require:
      - file: /etc/aliases
    - onchange_in:
      - file: /etc/aliases

{% if users|length %}
/etc/postfix/generic_maps:
  file:
    - managed
    - source: salt://mail/postfix/generic_maps.conf
    - user: root
    - group: mail
    - mode: 644
    - template: jinja
    - context:
      admin_email: {{ admin_email }}
      mail_domain: {{ mail_domain }}
      users: {{ users }}
    - require:
      - pkg: postfix

update_generic_maps:
  cmd.run:
    - name: /usr/sbin/postmap /etc/postfix/generic_maps
    - require:
      - file: /etc/postfix/generic_maps
    - onchange_in:
      - file: /etc/postfix/generic_maps

{% endif %} {# if users|length #}

{% if sender_rules|length %}
/etc/postfix/sender_canonical_maps:
  file:
    - managed
    - source: salt://mail/postfix/sender_canonical_maps.conf
    - user: root
    - group: mail
    - mode: 644
    - template: jinja
    - context:
      sender_rules: {{ sender_rules }}
    - require:
      - pkg: postfix

update_sender_canonical_maps:
  cmd.run:
    - name: /usr/sbin/postmap /etc/postfix/sender_canonical_maps
    - require:
      - file: /etc/postfix/sender_canonical_maps
    - onchange_in:
      - file: /etc/postfix/sender_canonical_maps

{% endif %} {# if sender_rules|length #}
{% endif %} {# if mail_server #}
{% endif %} {# if server_meta #}
