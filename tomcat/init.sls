{% set workflow = pillar.get('workflow', None) %}
{% set tomcat = pillar.get('tomcat', None) %}
{% if workflow or tomcat %}

tomcat9:
  pkg:
    - installed
    - require:
      - pkg: openjdk

# /etc/default/tomcat8:
#   file.managed:
#     - source: salt://tomcat/tomcat8
#     - user: root
#     - group: root
#     - require:
#       - pkg: tomcat8
#
# /usr/share/tomcat8/bin/setenv.sh:
#   file.managed:
#     - source: salt://tomcat/setenv.sh
#     - user: root
#     - group: root
#     - mode: 644
#     - require:
#       - pkg: tomcat8

{% endif %}
