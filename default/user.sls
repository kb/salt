{# get user data from the pillar #}
{% set users = pillar.get('users', {}) %}
{% set additional_users = pillar.get('additional_users', {}) %}
{% set php = pillar.get('php', None) -%}

{# Merge Jinja dictionaries? #}
{# https://stackoverflow.com/questions/2703013/how-can-i-modify-merge-jinja2-dictionaries?rq=1 #}
{# Don't use '_dummy'!! #}
{% set _dummy = users.update(additional_users) %}

{# Only set-up the script if we have a user #}
{% if users|length %}

{% for user, settings in users.items() %}

{% set is_sudo = settings.get('sudo', None) %}
{% set keys = settings.get('keys', None) %}

{{ user }}-group:
  group.present:
    - gid: {{ settings.get('uid') }}
    - name: {{ user }}

{{ user }}:
  user.present:
    - fullname: {{ settings.get('fullname') }}
    - uid: {{ settings.get('uid') }}
    - groups:
      - {{ user }}
      {% if is_sudo %}
      - sudo
      - adm
      {% endif %}
      - users
      {% if php -%}
      - www-data
      {% endif %}
    - password: {{ settings.get('password') }}
    - enforce_password: True
    - shell: /bin/bash
    - require:
      - group: {{ user }}-group
      {% if php -%}
      - pkg: nginx
      {% endif %}

{% if keys %}
{{ user }}-ssh-key:
  ssh_auth:
    - manage
    - user: {{ user }}
    - require:
      - user: {{ user }}
    - ssh_keys:
      {% for key in keys %}
      - {{ key }}
      {% endfor %}
{% endif %}

/home/{{ user }}/repo/temp/backup-vim:
  file.directory:
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - user: {{ user }}
    - mode: 755
    - makedirs: True

/home/{{ user }}/bin:
  file.directory:
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - user: {{ user }}
    - mode: 755
    - makedirs: True

/home/{{ user }}/.bashrc:
  file:
    - managed
    - source: salt://default/.bashrc
    - template: jinja
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - user: {{ user }}

/home/{{ user }}/.inputrc:
  file:
    - managed
    - source: salt://default/.inputrc
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - user: {{ user }}

/home/{{ user }}/.vimrc:
  file:
    - managed
    - source: salt://default/.vimrc
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - user: {{ user }}

/home/{{ user }}/.tmux.conf:
  file:
    - managed
    - source: salt://default/.tmux.conf
    - user: {{ user }}
    - group: {{ user }}
    - require:
      - user: {{ user }}

{% endfor %}
{% endif %}

{# root user #}
/root/repo/temp/backup-vim:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/root/repo/temp/config.report:
  file:
    - managed
    - source: salt://default/config.report
    - template: jinja

/root/.bashrc:
  file:
    - managed
    - source: salt://default/.bashrc
    - template: jinja
    - user: root
    - group: root

/root/.inputrc:
  file:
    - managed
    - source: salt://default/.inputrc
    - user: root
    - group: root

/root/.vimrc:
  file:
    - managed
    - source: salt://default/.vimrc
    - user: root
    - group: root
