{% set salt_master = pillar.get("salt-master", False) %}
{% if salt_master %}
/etc/salt/master.d/state-verbose.conf:
  file:
    - managed
    - source: salt://salt/state-verbose.conf
    - user: root
    - group: root
    - mode: 644
{% endif %}

# remove old config files
/etc/apt/sources.list.d/salt.list:
    file.absent

/etc/apt/sources.list.d/saltstack.list:
    file.absent

