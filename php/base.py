{% block python_import %}
{% endblock python_import %}

{% set db_name = domain|replace('.', '_')|replace('-', '_') %}
{% if not db_user %}
{% set db_user = db_name %}
{% endif %}
DATABASE_NAME = "{{ db_name }}"
DATABASE_PASS = "{{ db_pass }}"
DATABASE_USER = "{{ db_user }}"
DOMAIN_NAME = "{{ domain }}"


def hard_drive(confirm=None):
    print()
    console.print("Hard Disk", style="bold yellow")
    total, used, free = shutil.disk_usage("/")
    console.print("   Total: {} GiB".format(total // (2**30)), style="cyan")
    console.print("    Used: {} GiB".format(used // (2**30)), style="cyan")
    console.print("    Free: {} GiB".format(free // (2**30)), style="cyan")
    if confirm:
        Prompt.ask("Enter to continue...", default="<Enter>")
    print()

{% block python_code %}
{% endblock python_code %}
