{% extends "php/base.py" -%}
{% block python_import %}
"""
Dump database and archive files from a live site.

.. note:: This script is created and managed by our Salt states.

"""
import pathlib
import shutil
import subprocess
import tarfile

from datetime import datetime
from rich.console import Console
from rich.pretty import pprint
from rich.prompt import Prompt

{% endblock python_import %}

{% block python_code %}
console = Console()


def dump_db(dump_folder, now):
    dump_file = dump_folder.joinpath(
        "{}_{}.dump.sql".format(DATABASE_NAME, now.strftime("%Y%m%d_%H%M"))
        # "{}_{}.dump.bz2".format(DATABASE_NAME, now.strftime("%Y%m%d_%H%M"))
    )
    command = (
        "mysqldump "
        "--user={} "
        "--password={} "
        "--no-tablespaces "
        "{} "
        "> {}"
        # "| bzip2 > {}"
    ).format(
        DATABASE_USER,
        DATABASE_PASS,
        DATABASE_NAME,
        dump_file,
    )
    console.print("    Dump: {}".format(command), style="green")
    console.print("      To: {}".format(dump_file), style="green")
    try:
        result = subprocess.run(command, capture_output=True, shell=True)
    except Exception as e:
        print()
        print("e")
        pprint(e, expand_all=True)
        print()
        raise Exception("Failed to run the 'mysqldump' process", e)
    if result.returncode == 0:
        console.print(" Success: {}".format(dump_file), style="yellow")
        console.print(result.stdout.decode("utf-8").strip())
    else:
        pprint(result)
        raise Exception("Child was terminated by signal: {}".format(result))


def dump_files(dump_folder, now):
    """Archive files in the WordPress folder.

    Code copied from:
    https://stackoverflow.com/questions/2032403/how-to-create-full-compressed-tar-file-using-python

    """
    archive_file = dump_folder.joinpath(
        "{}-{}.tar.gz".format(DOMAIN_NAME, now.strftime("%Y%m%d-%H%M"))
    )
    source_folder = pathlib.Path("/").joinpath(
        "home",
        "web",
        "repo",
        "project",
        DOMAIN_NAME,
        "live",
    )
    console.print(" Archive: {}".format(source_folder), style="green")
    console.print("      To: {}".format(archive_file), style="green")
    if source_folder.exists():
        with tarfile.open(archive_file, "w:gz") as tar:
            tar.add(source_folder, arcname="")
        console.print(" Success: {}".format(archive_file), style="yellow")
    else:
        raise Exception(
            "Source folder does not exist: {}".format(source_folder)
        )


def main():
    now = datetime.now()
    print()
    console.print(
        "Dump (and archive) {}.  Ticket 6109".format(DOMAIN_NAME),
        style="bold yellow",
    )
    hard_drive(confirm=True)
    folder = pathlib.Path.home().joinpath("repo", "dump")
    folder.mkdir(parents=False, exist_ok=True)
    dump_folder = folder.joinpath(now.strftime("%Y%m%d-%H%M"))
    dump_folder.mkdir(parents=False, exist_ok=False)
    dump_db(dump_folder, now)
    dump_files(dump_folder, now)
    hard_drive()


if __name__ == "__main__":
    main()
{% endblock python_code %}
