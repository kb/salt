{% set php = pillar.get('php', None) %}
{% if php %}

{% if grains['osmajorrelease'] == 22 %}
{% set php_version = "8.1" %}
{% else %}
{% set php_version = "7.4" %}
{% endif %}

{% set sites = pillar.get('sites', {}) %}

php-fpm:
  pkg:
    - installed

php{{ php_version }}-fpm:
  pkg:
    - installed
  service:
    - running

php:
  pkg:
    - installed
    - require:
      - pkg: php-fpm

# nextcloud
php-bcmath:
  pkg:
    - installed
    - require:
      - pkg: php

php-curl:
  pkg:
    - installed
    - require:
      - pkg: php

php-gd:
  pkg:
    - installed
    - require:
      - pkg: php

php-mbstring:
  pkg:
    - installed
    - require:
      - pkg: php

# nextcloud
php-gmp:
  pkg:
    - installed
    - require:
      - pkg: php

php-imagick:
  pkg:
    - installed
    - require:
      - pkg: php

# nextcloud
php-intl:
  pkg:
    - installed
    - require:
      - pkg: php

php-zip:
  pkg:
    - installed
    - require:
      - pkg: php

php-mysql:
  pkg:
    - installed
    - require:
      - pkg: php

php-xml:
  pkg:
    - installed
    - require:
      - pkg: php

/var/run/php:
  file.directory:
    - user: www-data
    - group: www-data
    - mode: 755
    - makedirs: False
    - require:
      - pkg: php-fpm

{# we always use a domain specific pool file - delete the generic one #}
/etc/php/{{ php_version }}/fpm/pool.d/www.conf:
  file.absent

{% for site, settings in sites.items() -%}
{% set db_pass = settings.get('db_pass', {}) -%}
{% set db_user = settings.get('db_user', False) -%}
{% set domain = settings.get('domain') -%}
{% set profile = settings.get('profile') -%}
{% set php_profile = settings.get('php_profile') -%}
{% set slowlog_timeout = settings.get('slowlog_timeout', 2) -%}
{% set testing = settings.get('testing', None) -%}

{# max_children is best set on a per site basis taking into account #}
{# other sites on the server and the size of the worker process #}
{# The worker process size is calculated summing the rss values from: #}
{# ps -e -o "rss,cmd" | grep php | grep -v "master process" #}
{# This is in KB, divide by 1024 to get MB  #}
{# Using this figure for the worker calculate the max_children by: #}
{# Allow for system: 500MB #}
{# Allow for mysql (if applicable): 20% of total memory #}
{# max children = (total mem - system - mysql) / average worker size #}
{# e.g in 4GB system with worker size of 200MB #}
{# max_children = (4096 - 500 - 820) / 200  = 13 #}
{# or a 1GB system with worker size of 50MB #}
{# max_children = (1024 - 500 - 205) / 50  = 6 #}
{% if testing -%}
{% set max_children = settings.get('php_max_children', 10) -%}
{% else %}
{% set max_children = settings.get('php_max_children', 75) -%}
{% endif %}

{% set start_servers = settings.get('php_start_servers', 2) -%}
{% set min_spare_servers = settings.get('php_min_spare_servers', 1) -%}
{% set max_spare_servers = settings.get('php_max_spare_servers', 3) -%}
{% set max_requests = settings.get('php_max_requests', 50) -%}

{% if profile == 'php' %}
{% if php_profile == "wordpress" %}
{# python module to dump and archive a wordpress site #}
dump-stage-readme-{{ site }}:
  file:
    - managed
    - name: /home/web/dev/managed/dump-stage/README.rst
    - source: salt://php/README.rst
    - user: web
    - group: web
    - makedirs: True
    - mode: 644

{% if testing -%}
{# python module to restore a wordpress site #}
/home/web/dev/managed/dump-stage/stage-{{ site|replace('.', '-') }}.py:
  file:
    - managed
    - source: salt://php/stage.py
    - user: web
    - group: web
    - mode: 644
    - makedirs: True
    - template: jinja
    - context:
      domain: {{ site }}
      db_pass: {{ db_pass }}
      db_user: {{ db_user }}

/home/web/dev/managed/dump-stage/dump-{{ site|replace('.', '-') }}.py:
    file.absent
{% else %}
{# python module to dump and archive a wordpress site #}
/home/web/dev/managed/dump-stage/dump-{{ site|replace('.', '-') }}.py:
  file:
    - managed
    - source: salt://php/dump.py
    - user: web
    - group: web
    - mode: 644
    - makedirs: True
    - template: jinja
    - context:
      domain: {{ site }}
      db_pass: {{ db_pass }}
      db_user: {{ db_user }}

/home/web/dev/managed/dump-stage/stage-{{ site|replace('.', '-') }}.py:
    file.absent
{% endif %} {# testing #}
{% endif %} {# wordpress #}

/etc/php/{{ php_version }}/fpm/pool.d/{{ site }}.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://php/fpm.conf
    - template: jinja
    - context:
      site: {{ site }}
      php_profile: {{ php_profile }}
      start_servers: {{ start_servers }}
      max_children: {{ max_children }}
      max_requests: {{ max_requests }}
      min_spare_servers: {{ min_spare_servers }}
      max_spare_servers: {{ max_spare_servers }}
      slowlog_timeout: {{ slowlog_timeout }}
    - require:
      - pkg: php-fpm

{% endif %}
{% endfor -%}

{# The numbers in front of the conf.d files is used for prioritizing the order in which the files load. #}
/etc/php/{{ php_version }}/fpm/conf.d/99-site-param.ini:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://php/99-site-param.ini
    - require:
      - pkg: php-fpm

/etc/systemd/system/php{{ php_version }}-fpm.service.d:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: False
    - require:
      - pkg: php-fpm

{# Files created by WordPress have the correct permissions #}
{# https://www.kbsoftware.co.uk/crm/ticket/6339/ #}
/etc/systemd/system/php{{ php_version }}-fpm.service.d/umask.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://php/umask.conf
    - require:
      - pkg: php-fpm

{% endif %}
