{% extends "php/base.py" -%}
{% block python_import %}
"""
Stage a WordPress site.

.. note:: This script is created and managed by our Salt states.

"""
import pathlib
import shutil
import subprocess
import tarfile

from datetime import datetime
from rich.console import Console
from rich.pretty import pprint
from rich.prompt import IntPrompt, Prompt

{% endblock python_import %}

{% block python_code %}
console = Console()


def restore_db(dump_folder):
    print()
    console.print("Database", style="bold yellow")
    console.print(
        (
            " Warning: Is '{}' an empty database - "
            "or are you happy to merge?"
        ).format(DATABASE_NAME),
        style="cyan",
    )
    print()
    Prompt.ask("Enter to continue...".format(DATABASE_NAME), default="<Enter>")
    print()
    sql_file = None
    for file_name in dump_folder.iterdir():
        if file_name.is_file():
            if file_name.suffix == ".sql":
                sql_file = file_name
                break
    if not sql_file:
        raise Exception("Cannot find 'sql' file in: {}".format(dump_folder))
    command = "mysql --user={} --password={} {} < {}".format(
        DATABASE_USER,
        DATABASE_PASS,
        DATABASE_NAME,
        sql_file,
    )
    console.print(" Restore: {}".format(command), style="green")
    console.print("     SQL: {}".format(sql_file), style="green")
    try:
        result = subprocess.run(command, capture_output=True, shell=True)
    except Exception as e:
        print()
        print("e")
        pprint(e, expand_all=True)
        print()
        raise Exception("Failed to run the 'sql' process", e)
    if result.returncode == 0:
        console.print(" Success: {}".format(sql_file), style="yellow")
        console.print(result.stdout.decode("utf-8").strip())
    else:
        pprint(result)
        raise Exception("Child was terminated by signal: {}".format(result))


def restore_files(dump_folder, now):
    print()
    console.print("Files", style="bold yellow")
    tar_file = None
    for file_name in dump_folder.iterdir():
        if file_name.is_file():
            if file_name.suffix == ".gz":
                tar_file = file_name
                break
    if not tar_file:
        raise Exception("Cannot find 'tar.gz' file in: {}".format(dump_folder))
    live_folder = pathlib.Path("/").joinpath(
        "home",
        "web",
        "repo",
        "project",
        DOMAIN_NAME,
        "live",
    )
    if live_folder.exists():
        archive_live_folder = pathlib.Path("/").joinpath(
            "home",
            "web",
            "repo",
            "project",
            DOMAIN_NAME,
            "live-until-{}".format(now.strftime("%Y%m%d-%H%M")),
        )
        console.print("  Rename: {}".format(live_folder), style="green")
        console.print("      To: {}".format(archive_live_folder), style="green")
        live_folder.rename(archive_live_folder)
        print()
    console.print(" Restore: {}".format(tar_file), style="green")
    console.print("      To: {}".format(live_folder), style="green")
    with tarfile.open(tar_file) as f:
        f.extractall(live_folder)
    console.print(" Success: {}".format(live_folder), style="yellow")
    return live_folder


def select_archive():
    dump_folder = pathlib.Path.home().joinpath("repo", "dump")
    folders = sorted([x for x in dump_folder.iterdir() if x.is_dir()])
    items = {}
    count = 0
    for folder in folders:
        count = count + 1
        items[count] = folder
    for number, folder in items.items():
        console.print("{}. {}".format(number, folder.name))
        for file_name in folder.iterdir():
            if file_name.is_file():
                console.print("   {}".format(file_name.name))
    print()
    number = int(
        IntPrompt.ask(
            "Which backup do you want to stage to '{}'?".format(DOMAIN_NAME),
            default=count,
            choices=[str(i) for i in items.keys()],
        )
    )
    return items[number]


def main():
    now = datetime.now()
    print()
    console.print("Stage a WordPress site.  Ticket 6109", style="bold yellow")
    hard_drive()
    dump_folder = select_archive()
    restore_db(dump_folder)
    live_folder = restore_files(dump_folder, now)
    print()
    hard_drive()
    print()
    wp_config = live_folder.joinpath("wp-config.php")
    console.print("Checklist", style="bold yellow")
    console.print("1. Update '{}'".format(wp_config), style="white")
    print()
    console.print("define('DB_NAME', '{}');".format(DATABASE_NAME))
    console.print("define('DB_USER', '{}');".format(DATABASE_USER))
    console.print("define('DB_PASSWORD', '{}');".format(DATABASE_PASS))
    print()
    console.print("define('WP_HOME', 'https://{}');".format(DOMAIN_NAME))
    console.print("define('WP_SITEURL', 'https://{}');".format(DOMAIN_NAME))
    print()
    console.print("define('WP_DEBUG', false);")
    print()
    console.print("/** https://www.kbsoftware.co.uk/docs/sys-wordpress-security.html#cron */")
    console.print("define('DISABLE_WP_CRON', true);")
    print()
    robots_txt = live_folder.joinpath("robots.txt")
    console.print(
        "2. Create the '{}' file to prevent the site being crawled:".format(robots_txt),
        style="white"
    )
    console.print("User-agent: *")
    console.print("Disallow: /")
    print()
    console.print("3. Remove all '.htaccess' files from the site.", style="white")
    console.print("https://www.kbsoftware.co.uk/docs/sys-wordpress-security.html#htaccess", style="white")
    print()
    console.print("4. Run the salt states to fix permissions.", style="white")
    print()
    print()
    console.print("5. Update the 'upload_path' in the database:", style="white")
    console.print("https://www.kbsoftware.co.uk/docs/sys-wordpress.html#media-library", style="white")
    print()
    print()
    console.print("6. Update the 'siteurl' and 'home in the database:", style="white")
    console.print("https://www.kbsoftware.co.uk/docs/sys-wordpress-issues.html#options-url", style="white")
    print()
    print()
    console.print("7. For any redirect issues, see:", style="white")
    console.print("https://www.kbsoftware.co.uk/docs/sys-wordpress-issues.html#redirect", style="white")
    print()


if __name__ == "__main__":
    main()
{% endblock python_code %}
