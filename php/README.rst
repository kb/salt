Dump data and files - Create staging site
*****************************************

- We create a ``dump-`` script for *live* sites and ``stage-`` scripts for test
  sites (``testing: True`` in the ``pillar``).
- The ``dump-`` scripts save the ``sql`` and ``tar.gz`` files to the
  ``/home/web/repo/dump/`` folder.
- Unless you create a new database for the staging site, the MySQL restore will
  merge the tables etc...
- The ``tar.gz`` archive will be restored to the ``live`` folder for the test
  site.  If a ``live`` folder already exists, it will be renamed e.g.
  ``/home/web/repo/project/hatherleigh.kbsoftware.co.uk/live-until-20220408-1152``

::

  cd /home/web/dev/managed/dump-stage/
  python3 -m venv venv
  source venv/bin/activate

  pip install rich

To dump / archive a site::

  python dump-www-kbsoftware-co-uk.py

To stage a site::

  python stage-hatherleigh-kbsoftware-co-uk.py
