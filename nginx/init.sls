{% set nginx = pillar.get('nginx', None) %}
{% if nginx %}

{% set devpi = pillar.get('devpi', False) -%}
{% set nginx_services = pillar.get('nginx_services', {}) %}
{% set r = pillar.get('r', False) -%}
{% set server_meta = pillar.get('server_meta', None) %}
{% if server_meta %}
{% set default_site = server_meta.get("default_site", {}) %}
{% set configure_default_site = default_site.get("enable", True) %}
{% set extra_nginx_http = server_meta.get("nginx", []) %}
{% else %}
{% set configure_default_site = True %}
{% set default_site = {} %}
{% set extra_nginx_http = [] %}
{% endif %}
{% set sites = pillar.get('sites', {}) %}

nginx:
  pkg.installed: []
  service:
    - running
    - reload: True
    - watch:
       - pkg: nginx
       - file: /etc/nginx/nginx.conf
       - file: /etc/nginx/include/*

libnginx-mod-http-headers-more-filter:
  pkg.installed:
    - require:
      - pkg: nginx

# disable default site
/etc/nginx/sites-enabled/default:
  file.absent
/etc/nginx/sites-available/default:
  file.absent

nginx.conf:
  file:                                         # state declaration
    - managed                                   # function
    - name: /etc/nginx/nginx.conf
    - source: salt://nginx/nginx.conf
    - template: jinja
    - context:
      devpi: {{ devpi }}
      nginx: {{ nginx }}
      extra_nginx_http: {{ extra_nginx_http }}
      configure_default_site: {{ configure_default_site }}
      nginx_services: {{ nginx_services }}
      r: {{ r }}
      sites: {{ sites }}
    - require:                                  # requisite declaration
      - pkg: nginx                              # requisite reference

/etc/nginx/include:
  file.directory:
    - mode: 755
    - require:
      - pkg: nginx

{# strong Diffie-Hellman certificate #}
/etc/ssl/certs/dhparam.pem:
  cmd.run:
    - name: openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
    - unless: test -f /etc/ssl/certs/dhparam.pem

{% for domain, settings in sites.items() %}

/etc/nginx/include/{{ domain }}.conf:
  file:
    - managed
    - source: salt://nginx/include-site.conf
    - template: jinja
    - context:
      domain: {{ domain }}
      r: {{ r }}
      settings: {{ settings }}
    - require:
      - file: /etc/nginx/include

# Folder for certificates
# http://library.linode.com/web-servers/nginx/configuration/ssl
/srv/ssl/{{ domain }}/:
  file.directory:
    - user: www-data
    - group: www-data
    - mode: 400
    - makedirs: True
    - recurse:
      - user
      - group
    - require:
      - pkg: nginx

{% endfor %} # domain, settings

{% if configure_default_site %}
# default site
/etc/nginx/include/default.conf:
  file:
    - managed
    - source: salt://nginx/include-default.conf
    - template: jinja
    - context:
      default_site: {{ default_site }}
    - require:
      - file: /etc/nginx/include
{% endif %}

/srv/ssl/default:
  file.directory:
    - user: www-data
    - group: www-data
    - mode: 755
    - makedirs: True

/srv/ssl/default/default.crt:
  file:
    - managed
    - user: www-data
    - group: www-data
    - mode: 400
    - makedirs: False
    - contents_pillar: nginx:ssl:crt
    - require:
      - file: /srv/ssl/default

/srv/ssl/default/default.key:
  file:
    - managed
    - user: www-data
    - group: www-data
    - mode: 400
    - contents_pillar: nginx:ssl:key
    - makedirs: False
    - require:
      - file: /srv/ssl/default/default.crt

/srv/www/default:
  file.directory:
    - user: www-data
    - group: www-data
    - mode: 755
    - makedirs: True

/srv/www/default/index.html:
  file:
    - managed
    - source: salt://nginx/index.html
    - user: www-data
    - group: www-data
    - mode: 400
    - makedirs: False
    - require:
      - file: /srv/www/default

# devpi
{% if devpi -%}
/etc/nginx/include/devpi.conf:
  file:
    - managed
    - source: salt://nginx/include-devpi.conf
    - template: jinja
    - context:
      devpi: {{ devpi }}
    - require:
      - file: /etc/nginx/include
{% endif %} # devpi

{% endif %}
