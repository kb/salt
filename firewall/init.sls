{% set firewall = pillar.get('firewall', None) %}
{% if firewall %}
{% set interfaces = firewall.get('interfaces', {}) %}
{% set ports = [] %}
{% set monitor = firewall.get('monitor', []) %}
{% set web = firewall.get('web', []) %}
{% set server_meta = pillar.get('server_meta', None) %}
{% if server_meta %}
{% set custom_firewall = server_meta.get('firewall', []) %}
{% if custom_firewall %}
{% set monitor = custom_firewall.get('monitor', []) %}
{% set web = custom_firewall.get('web', []) %}
{% set ports = custom_firewall.get('ports', []) %}
{% set interfaces = custom_firewall.get('interfaces', {}) %}
{% endif %}
{% endif %}

/etc/iptables:
  file.directory:
    - user: root
    - group: root
    - mode: 744
    - makedirs: False

iptables-persistent:
  pkg.installed

netfilter-persistent:
  pkg.installed:
    - require:
      - pkg: iptables-persistent

iptables-conf:
  file.managed:
    - source: salt://firewall/rules.v4
    - name: /etc/iptables/rules.v4
    - user: root
    - template: jinja
    - group: root
    - mode: 644
    - context:
      monitor: {{ monitor }}
      web: {{ web }}
      ports: {{ ports }}
      interfaces: {{ interfaces }}

iptables6-conf:
  file.managed:
    - source: salt://firewall/rules.v6
    - name: /etc/iptables/rules.v6
    - user: root
    - template: jinja
    - group: root
    - mode: 644
    - context:
      monitor: {{ monitor }}
      web: {{ web }}
      ports: {{ ports }}
      interfaces: {{ interfaces }}

netfilter-persistent-service:
  service:
    - running
    - name: netfilter-persistent
    - enable: True
    - restart: True
    - watch:
      - file: /etc/iptables/rules.v4
    - require:
      - pkg: netfilter-persistent

/etc/rsyslog.d/19-iptables.conf:
  file.managed:
    - source: salt://firewall/iptables-log.conf
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
{% endif %}
