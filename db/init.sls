{% set django = pillar.get('django', None) %}
{% set mysql_client = pillar.get('mysql_client', None) %}
{% set sites = pillar.get('sites', {}) %}
{% if mysql_client %}

libmysqlclient-dev:
  pkg.installed

{% endif %} # mysql_client


{% set mysql_server = pillar.get('mysql_server', {}) -%}
{% if mysql_server %}

{% set root_password_hash = mysql_server.get('root_password_hash', {}) -%}
{% set allow_remote = mysql_server.get('allow_remote', False) -%}

mysql-server:
  pkg:
    - installed
    - pkgs:
      - mysql-server
      - python3-mysqldb

mysql-service:
  service:
    - running
    - name: mysql
    - enable: True
    - watch:
      - pkg: mysql-server
{#
 # This sets the password for the root user initially time but fails on each
 # subsequent state.highstate
  mysql_user.present:
    - name: root
    - password_hash: '{{ root_password_hash }}'
    - require: 
      - service: mysql
      - pkg: python3-mysqldb
#}
/etc/mysql/mysql.cnf:
  file.managed:
    - source: salt://db/my.cnf
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      allow_remote: {{ allow_remote }}
    - require:
      - pkg: mysql-server

{% endif %}

{% set postgres_server = pillar.get('postgres_server', {}) -%}
{% set postgres_settings = pillar.get('postgres_settings', {}) %}
{% set workflow = pillar.get('workflow', False) %}
{% set pg_encryption = postgres_settings.get('encryption', 'md5') %}

{%- if grains['osmajorrelease'] == 24 -%}
{% set pg_version = '16' %}
{%- elif grains['osmajorrelease'] == 22 -%}
{% set pg_version = '14' %}
{%- elif grains['osmajorrelease'] == 21 -%}
{% set pg_version = '13' %}
{%- elif grains['osmajorrelease'] == 20 -%}
{% set pg_version = '12' %}
{%- elif grains['osmajorrelease'] == 18 -%}
{% set pg_version = '10' %}
{%- elif grains['osmajorrelease'] == 16 -%}
{% set pg_version = '9.5' %}
{% endif %}


{% if postgres_server or postgres_settings %}
libpq-dev:
  pkg.installed

{% if postgres_settings.get('bouncer', False) %}
pgbouncer:
  pkg:
    - installed
  service:
    - running
    - watch:
      - file: /etc/pgbouncer/pgbouncer.ini
      - file: /etc/pgbouncer/userlist.txt

/etc/pgbouncer/pgbouncer.ini:
  file:
    - managed
    - source: salt://db/pgbouncer.ini
    - user: postgres
    - group: postgres
    - mode: 640
    - template: jinja
    - context:
      postgres_settings: {{ postgres_settings }}
      sites: {{ sites }}
    - require:
      - pkg: pgbouncer

/etc/pgbouncer/userlist.txt:
  file:
    - managed
    - source: salt://db/userlist.txt
    - user: postgres
    - group: postgres
    - mode: 640
    - template: jinja
    - context:
      postgres_settings: {{ postgres_settings }}
      sites: {{ sites }}
    - require:
      - pkg: pgbouncer
{% endif %} {# bouncer #}

{# do we need a certificate for postgres access #}
{# https://docs.microsoft.com/en-us/azure/postgresql/concepts-ssl-connection-security #}
{% set postgres_ssh = postgres_settings.get('postgres_ssh', False) %}
{% if postgres_ssh %}
/home/web/.postgresql:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - user: web

{# https://docs.microsoft.com/en-us/azure/postgresql/concepts-ssl-connection-security #}
/home/web/.postgresql/root.crt:
  file:
    - managed
    - user: web
    - group: web
    - source: salt://db/BaltimoreCyberTrustRoot.crt.pem
    - require:
      - file: /home/web/.postgresql
{% endif -%} {# if postgres_ssh #}

postgresql-client-{{ pg_version }}:
  pkg.installed

{% endif %}

{% if postgres_server %}

postgresql:
  pkg:
    - installed
  service:
    - running

/etc/postgresql/{{ pg_version }}/main/pg_hba.conf:
  file:
    - managed
    - source: salt://db/pg_hba.conf
    - user: postgres
    - group: postgres
    - mode: 640
    - template: jinja
    - context:
      postgres_server: {{ postgres_server }}
      workflow: {{ workflow }}
      pg_encryption: {{ pg_encryption }}

    - require:
      - pkg: postgresql

/etc/postgresql/{{ pg_version }}/main/postgresql.conf:
  file:
    - managed
    - source: salt://db/postgresql.conf
    - user: postgres
    - group: postgres
    - mode: 644
    - template: jinja
    - context:
      postgres_settings: {{ postgres_settings }}
      pg_version: {{ pg_version }}
    - require:
      - pkg: postgresql


{% set block_storage_folder = postgres_server.get('block_storage_folder', None) %}
{% if block_storage_folder %}
# data on Rackspace cloud block storage
{{ block_storage_folder }}:
  file.directory:
    - user: postgres
    - group: postgres
    - mode: 700
    - makedirs: True
{% endif %}

{% endif %}
