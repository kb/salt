{% set redis = pillar.get('redis', None) %}
{% if redis %}

redis-server:
  pkg:
    - installed
  service:
    - running
    - watch:
      - file: /etc/redis/redis.conf

{% set private_network = pillar.get('private_network', False) -%}
{% set redis_protected = redis.get('protected', True) -%}

/etc/redis/redis.conf:
  file:
    - managed
    - source: salt://redis/redis{% if grains['osmajorrelease'] < 20 %}-before-20.04{% endif %}.conf
    - user: redis
    - group: redis
    - template: jinja
    - context:
      private_network: {{ private_network }}
      redis_protected: {{ redis_protected }}

{% endif %} {# redis #}
