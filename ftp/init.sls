{% set sites = pillar.get('sites', {}) %}

{% if sites %}
{# Do any of the sites on this server have FTP #}
{# nasty workaround from #}
{# http://stackoverflow.com/questions/9486393/jinja2-change-the-value-of-a-variable-inside-a-loop #}
{% set vars = {'has_ftp': False} %}
{% for site, settings in sites.items() %}
  {% if settings.get('ftp', None) %}
    {% if vars.update({'has_ftp': True}) %} {% endif %}
  {% endif %}
{% endfor %}
{% set has_ftp = vars.has_ftp %}

{# Only set-up ftp if we have a site using ftp #}
{% if has_ftp %}
  vsftpd:
    pkg:
      - installed
    service:
      - running
      - watch:
         - file: /etc/vsftpd.conf
         - file: /etc/vsftpd.userlist

  /etc/vsftpd.conf:
    file.managed:
      - source: salt://ftp/vsftpd.conf
      - user: root
      - group: root
      - mode: 644
      - require:
        - pkg: vsftpd

  /etc/vsftpd.userlist:
    file.managed:
      - source: salt://ftp/vsftpd.userlist
      - template: jinja
      - context:
        sites: {{ sites }}
      - require:
        - pkg: vsftpd

  /home/web/repo/ftp:
    file.directory:
      - user: web
      - group: web
      - mode: 755
      - require:
        - file: /home/web/repo
{% endif %}

{% for domain, settings in sites.items() %}
{% if settings.get('ftp', None) %}
{% set env = settings['env'] -%}
{# for ftp uploads #}
  /home/web/repo/ftp/{{ domain|replace('.', '_') }}:
    file.directory:
      - user: web
      - group: web
      - mode: 755
      - require:
        - file: /home/web/repo/ftp

  ftp_group_{{ env['ftp_user_name'] }}:
    group.present:
      - name: {{ env['ftp_user_name'] }}
      - gid: {{ env['ftp_user_id'] }}
      - system: True

  ftp_user_{{ env['ftp_user_name'] }}:
    user.present:
      - name: {{ env['ftp_user_name'] }}
      - uid: {{ env['ftp_user_id'] }}
      - groups:
        - {{ env['ftp_user_name'] }}
      - password: {{ env['ftp_password'] }}
      - shell: /bin/false
      - require:
        - group: ftp_group_{{ env['ftp_user_name'] }}

  /home/{{ env['ftp_user_name'] }}:
    file.directory:
      - user: {{ env['ftp_user_name'] }}
      - group: web
      - require:
        - user: {{ env['ftp_user_name'] }}
      - mode: 555

  {# folder for 'watch_ftp_folder.py' to watch #}
  /home/{{ env['ftp_user_name'] }}/watch:
    file.directory:
      - user: {{ env['ftp_user_name'] }}
      - group: web
      - require:
        - file: /home/{{ env['ftp_user_name'] }}
      - mode: 755

  {# folder for ftp upload #}
  /home/{{ env['ftp_user_name'] }}/site:
    file.directory:
      - user: {{ env['ftp_user_name'] }}
      - group: web
      - require:
        - user: {{ env['ftp_user_name'] }}
      - mode: 755

  /home/{{ env['ftp_user_name'] }}/site/static:
    file.directory:
      - user: {{ env['ftp_user_name'] }}
      - group: web
      - require:
        - file: /home/{{ env['ftp_user_name'] }}/site
      - mode: 755

  /home/{{ env['ftp_user_name'] }}/site/templates:
    file.directory:
      - user: {{ env['ftp_user_name'] }}
      - group: web
      - require:
        - file: /home/{{ env['ftp_user_name'] }}/site
      - mode: 755

  /home/{{ env['ftp_user_name'] }}/site/templates/templatepages:
    file.directory:
      - user: {{ env['ftp_user_name'] }}
      - group: web
      - require:
        - file: /home/{{ env['ftp_user_name'] }}/site/templates
      - mode: 755

  {# symlink uploads to site folder #}
  /home/web/repo/ftp/{{ domain|replace('.', '_') }}/site:
    file.symlink:
      - target: /home/{{ env['ftp_user_name'] }}/site
      - require:
        - file: /home/web/repo/ftp/{{ domain|replace('.', '_') }}
        - file: /home/{{ env['ftp_user_name'] }}/site

  /home/web/repo/supervisor:
    file.directory:
      - user: web
      - group: web
      - mode: 755
      - makedirs: False

  /home/web/repo/supervisor/{{ domain|replace('.', '_') }}:
    file.directory:
      - user: web
      - group: web
      - mode: 755
      - makedirs: False


  /home/web/repo/supervisor/{{ domain|replace('.', '_') }}/watch_ftp_folder:
    file.directory:
      - user: web
      - group: web
      - mode: 755
      - makedirs: False

  /home/web/repo/supervisor/{{ domain|replace('.', '_') }}/watch_ftp_folder/venv_watch_ftp_folder:
    virtualenv.manage:
      - index_url: https://pypi.python.org/simple/
      - requirements: salt://ftp/requirements.txt
      - user: web
      {% if grains['osmajorrelease'] == 20 -%}
      - venv_bin: /usr/bin/pyvenv-3.6
      {% elif grains['osmajorrelease'] == 18 -%}
      - venv_bin: /usr/bin/pyvenv-3.6
      {% elif grains['osmajorrelease'] == 16 -%}
      - venv_bin: /usr/bin/pyvenv-3.5
      {% endif -%}
      - require:
        - file: /home/web/repo/supervisor/{{ domain|replace('.', '_') }}/watch_ftp_folder
        - pkg: python3-venv
        - pkg: python3-virtualenv

  {# watch files created in the 'watch' folder and tell the app #}
  /home/web/repo/supervisor/{{ domain|replace('.', '_') }}/watch_ftp_folder/watch_ftp_folder.py:
    file.managed:
      - source: salt://ftp/watch_ftp_folder.py
      - context:
        domain: {{ domain }}
        ftp_user_name: {{ env['ftp_user_name'] }}
      - template: jinja
      - user: web
      - group: web
      - mode: 755
      - makedirs: False

{% endif %}
{% endfor %}
{% endif %}
