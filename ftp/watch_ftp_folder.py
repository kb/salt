# -*- encoding: utf-8 -*-
"""
Watch a folder.  Upload new files to Alfresco.

Changelog
=========

2017
----

We are not using FTP upload for Django templates, so I am updating the script
to copy files to an Alfresco server.

We were using watchdog (see code from 2014), but watchdog does not know when a
file has finished copying.  I have searched for ideas, and it seems most of
them are flawed.  People suggest watching the file size - when the file size
stops increasing, then the file has finished copying.  This doesn't feel safe
to me.  ``inotify`` seems a more elegant solution, but apparently, some file
copy operations send multiple ``CLOSE_WRITE`` events.

I am now attempting to use ``inotify``, but all python ``inotify`` packages are
complicated.  ``inotify-simple`` is simple to use, so I will try it.

.. warning:: This code will not work on Windows because it doesn't have
             ``inotify``.

Sample code copied from:
http://inotify-simple.readthedocs.io/en/latest/

2014
----

Code was used to set permissions on files after upload by an FTP server.  I
think the files were Django templates which could be incorporated into the
site.

Original code copied from:
http://brunorocha.org/python/watching-a-directory-for-file-changes-with-python.html

"""
import json
import os
import requests
import stat
import sys
import time
import urllib.parse

from inotify_simple import INotify, flags, masks


API_VERSION = '0.1'


class WatchError(Exception):

    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr('%s, %s' % (self.__class__.__name__, self.value))


def chown(src_path):
    if os.path.isfile(src_path):
        out('         is_file: True')
        try:
            os.chmod(
                src_path,
                stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH
            )
        except OSError as e:
            out(e)
    else:
        out('          is_dir: True')
        try:
            os.chmod(
                src_path,
                stat.S_IREAD | stat.S_IWRITE | stat.S_IEXEC | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH
            )
        except OSError as e:
            out(e)


def env(key):
    try:
        return os.environ[key]
    except KeyError:
        raise WatchError(
            "Missing environment variable '{}'".format(key)
        )


def log_error(name, verb, url, response):
    message = "{} {} Data: {}".format(
        url,
        response.status_code,
        response.text,
    )
    raise WatchError("Cannot {} json data: {}".format(verb, message))


def post_json(url, token, folder, file_name):
    path_file = os.path.join(folder, file_name)
    chown(path_file)
    files = {
        'file_upload': (
            file_name,
            open(path_file, 'rb'),
        )
    }
    headers = {'Authorization': 'Token {}'.format(token)}
    response = requests.post(
        url,
        headers=headers,
        files=files,
    )
    if response.status_code == 201:
        out('            done: {}'.format(path_file))
        return response.json()
    elif response.status_code == 401:
        log_error('post_json', 'POST', url, response)
        raise WatchError(
            "Unauthorised: '{}: {}".format(response.reason, url)
        )
    elif response.status_code == 404:
        log_error('post_json', 'POST', url, response)
        raise WatchError(
            "URL: '{}: {}".format(response.reason, url)
        )
    else:
        log_error('post_json', 'POST', url, response)
        raise WatchError(
            "URL: '{}: {}".format(response.reason, url)
        )


def json_headers(token):
    return {
        'Content-type': 'application/json',
        'Authorization': 'Token {}'.format(token),
    }


def login(url, user_name, password):
    """login using a form post."""
    token = None
    data = {'username': user_name, 'password': password}
    login_url = url_login(url)
    try:
        response = requests.post(login_url, data=data)
    except requests.ConnectionError as e:
        raise WatchError('Cannot connect to {}'.format(login_url)) from e
    if response.status_code == 200:
        token = response.json().get('token')
    else:
        raise WatchError('Invalid Login ({}: {}) {}'.format(
            response.status_code,
            response.reason,
            response.text[:100]
        ))
    return token


def out(message):
    print(message, flush=True)


def url_api(url, path):
    """Return the API URL.

    e.g. https://www.hatherleigh.info/api/0.1/

    """
    if not url.endswith('/'):
        url = url + '/'
    if path.startswith('/'):
        path = path[1:]
    url = urllib.parse.urljoin(url, path)
    if not url.endswith('/'):
        url = url + '/'
    print(url)
    return url


def url_login(url):
    """Return the login URL."""
    if not url.endswith('/'):
        url = url + '/'
    return '{}token/'.format(url)


class MyHandler:

    def __init__(self, url_api, url_path, username, password):
        self.url_api = url_api
        self.url_path = url_path
        self.username = username
        self.password = password

    def on_created(self, folder, file_name):
        token = login(self.url_api, self.username, self.password)
        data = post_json(
            url_api(self.url_api, self.url_path),
            token,
            folder,
            file_name,
        )
        os.remove(os.path.join(folder, file_name))
        out('          remove: {}'.format(file_name))


if __name__ == "__main__":
    out('')
    out('watch_ftp_folder: starting...')
    api_path = env('API_PATH')
    api_url = env('API_URL')
    password = env('API_PASSWORD')
    username = env('API_USERNAME')
    folder = env('FTP_WATCH_FOLDER')
    if not os.path.exists(folder):
        raise WatchError("Folder '{}' does not exist".format(folder))
    out('        watching: {}'.format(folder))
    inotify = INotify()
    watch_flags = flags.CLOSE_WRITE
    wd = inotify.add_watch(folder, watch_flags)
    while True:
        for event in inotify.read():
            for flag in flags.from_mask(event.mask):
                if flag == flags.CLOSE_WRITE:
                    file_name = event.name
                    out('     CLOSE_WRITE: {}'.format(file_name))
                    handler = MyHandler(api_url, api_path, username, password)
                    handler.on_created(folder, file_name)
