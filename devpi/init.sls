{% set devpi = pillar.get('devpi', None) %}

{% if devpi %}

/home/web/repo/devpi:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - require:
      - file: /home/web/repo

/home/web/repo/devpi/data:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - require:
      - file: /home/web/repo/devpi

{# https://www.kbsoftware.co.uk/docs/issues.html#virtualenv-managed #}
{% if grains['osmajorrelease'] == 20 -%}
/home/web/repo/devpi/venv_devpi:
  virtualenv.managed:
    - index_url: https://pypi.org/simple
    - runas: web
    - user: web
    - requirements: salt://devpi/requirements.txt
    - require:
      - pkg: python3-venv
      - pkg: python3-virtualenv
      - file: /home/web/repo/devpi
{% endif -%}

{% endif %}
