{% set firewall = pillar.get('firewall', None) %}
{% set ns = namespace(is_wordpress=False) %}
{% set php = pillar.get('php', None) %}
{% set sites = pillar.get('sites', {}) %}

{% if firewall %}
/etc/logrotate.d/iptables:
  file.managed:
    - source: salt://logrotate/iptables
    - user: root
    - group: root
    - require:
      - pkg: iptables-persistent
{% endif %} {# firewall #}

{% for domain, settings in sites.items() %}
{% set php_profile = settings.get('php_profile', None) -%}
{% if php_profile == "wordpress" %}
{% set ns.is_wordpress = True %}
{% endif %}
{% endfor %}

{# logrotate.set fails - issue not fixed #}
{# https://github.com/saltstack/salt/issues/48125 #}
{% if ns.is_wordpress == True %}

/etc/logrotate.d/wordpress:
  file.managed:
    - source: salt://logrotate/wordpress
    - user: root
    - group: root
    - require:
      - file: /var/log/wordpress

{% endif %} # is_wordpress

{% if php %}
{% if grains['osmajorrelease'] == 22 %}
{% set php_version = "8.1" %}
{% else %}
{% set php_version = "7.4" %}
{% endif %}

/etc/logrotate.d/php{{ php_version}}-fpm:
  file.managed:
    - source: salt://logrotate/php-fpm
    - user: root
    - group: root
    - template: jinja
    - context:
      php_version: {{ php_version }}
    - require:
      - pkg: php-fpm
{% endif %} # php

/etc/systemd/journald.conf.d/journald.conf:
  file.managed:
    - source: salt://logrotate/journald.conf
    - user: root
    - group: root
    - mode: 644
    - makedirs: True

