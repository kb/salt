{% set alfresco = pillar.get('alfresco', False) %}
{% set backup_config = pillar.get('backup', {}) %}
{% set sites = pillar.get('sites', {}) %}
{% set workflow = pillar.get('workflow', None) %}

{% if alfresco %}

# for postgres
postgresql-client:
  pkg.installed


# https://gitlab.com/kb/root-to-share-redirect
/opt/alfresco-community/tomcat/webapps/ROOT.war:
  file.managed:
    - source: salt://alfresco/ROOT.war
    - user: root
    - group: root


# customised share created by https://gitlab.com/kb/productiviticloud_share
/opt/alfresco-community/tomcat/webapps/share.war:
  file.managed:
    - source: salt://alfresco/share.war
    - user: root
    - group: root

# script to run from cron which rotates logs
/home/web/opt/alfresco-log-rotate.sh:
  file:
    - managed
    - source: salt://alfresco/alfresco-log-rotate.sh
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/opt
      - user: web

{% for domain, settings in sites.items() %}
/opt/alfresco-community/tomcat/shared/classes/alfresco-global.properties:
  file.managed:
    - source: salt://alfresco/alfresco-global.properties
    - user: root
    - group: root
    - template: jinja
    - context:
      settings: {{ settings }}
      domain: {{ domain }}

{% if backup_config|length %}
/home/web/opt/alfresco-bart.sh:
  file:
    - managed
    - source: salt://alfresco/alfresco-bart.sh
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/opt
      - user: web

/home/web/opt/alfresco-bart.properties:
  file:
    - managed
    - source: salt://alfresco/alfresco-bart.properties
    - user: web
    - group: web
    - mode: 444
    - template: jinja
    - makedirs: True
    - context:
      backup_config: {{ backup_config }}
      domain: {{ domain }}
      settings: {{ settings }}
    - require:
      - file: /home/web/opt
      - user: web
{% endif %} # backup_config
{% endfor %} # domain, settings

{% endif %}
