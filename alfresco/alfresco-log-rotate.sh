ALFRESCO_LOG_DIR=/opt/alfresco-community
ALFRESCO_TOMCAT_LOG_DIR=/opt/alfresco-community/tomcat/logs
cd ${ALFRESCO_TOMCAT_LOG_DIR}

if [ "$(pwd)" != "${ALFRESCO_TOMCAT_LOG_DIR}" ]; then
    echo "****************************************************************************************"
    echo "* CANNOT CHANGE TO THE ALFRESCO TOMCAT LOG DIRECTORY ${ALFRESCO_TOMCAT_LOG_DIR} *"
    echo "****************************************************************************************"
else
	# delete older than 30 days
	find . -name localhost_access_log\* -mtime +30 -delete

	# compress older than yesterday
	for log in $(find . -name localhost_access_log\* ! -newermt yesterday ! -name \*.gz -print); do
		gzip $log
	done
fi

cd ${ALFRESCO_LOG_DIR}

if [ "$(pwd)" != "${ALFRESCO_LOG_DIR}" ]; then
    echo "************************************************************************"
    echo "* CANNOT CHANGE TO THE ALFRESCO LOG DIRECTORY ${ALFRESCO_LOG_DIR} *"
    echo "************************************************************************"
    exit 2
else
	mkdir -p logs
	# keep alfresco directory clean move archived to logs directory
    for log in $(ls -1 alfresco.log.2* share.log.2* solr.log.2* 2> /dev/null); do
        mv ${log} logs
    done

	# delete older than 90 days
	find logs -mtime +90 -delete

	# compress older than yesterday
	for log in $(find logs ! -newermt yesterday ! -name \*.gz -print); do
		gzip $log
	done
fi


