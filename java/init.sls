{% set keycloak = pillar.get('keycloak', None) %}
{% set search = pillar.get('search', None) %}
{% set workflow = pillar.get('workflow', None) %}
{% set tomcat = pillar.get('tomcat', None) %}

{% if keycloak or search or workflow or tomcat %}

openjdk:
  pkg.installed:
    - pkgs:
      - openjdk-8-jre-headless

{% endif %}
