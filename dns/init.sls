{% set commented_out = True %}
{% set server_meta = pillar.get('server_meta', None) %}
{% if server_meta %}
{% set dns = server_meta.get('dns', {}) %}
{% if dns %}
{% set zones = dns.get('zones', []) %}
{# install packages #}
bind9:
  pkg.installed: []
  service:
    - running
    - watch:
      - file: /etc/bind/*

bind9utils:
  pkg.installed

bind9-host:
  pkg.installed

/etc/bind/named.conf.options:
  file.managed:
    - source: salt://dns/named.conf.options
    - user: root
    - group: bind
    - mode: 644
    - template: jinja
    - context:
      forwarders: {{ dns.get("forwarders", []) }}
      allowed_ips: {{ dns.get("allowed_ips", []) }}
    - require:
      - pkg: bind9

/etc/bind/named.conf.local:
  file.managed:
    - source: salt://dns/named.conf.local
    - user: root
    - group: bind
    - mode: 644
    - template: jinja
    - context:
      zones: {{ zones }}
    - require:
      - pkg: bind9

{% for zone_info in zones %}
/etc/bind/{{ zone_info['name'] }}:
  file.managed:
    - source: salt://dns/db.conf
    - user: root
    - group: bind
    - mode: 644
    - template: jinja
    - context:
      serial: {{ zone_info['serial'] }}
      zone: {{ zone_info['zone'] }}
      primary_server: {{ zone_info['primary_server'] }}
      email_address: {{ zone_info['email_address'] }}
      entries: {{ zone_info['entries'] }}
    - require:
      - pkg: bind9
{% if not commented_out %}
{% endif %}
{% endfor %} {# for dns_file in dns #}
{% endif %} {# if dns #}
{% endif %} {# if server_meta #}
