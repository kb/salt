{% set chat = pillar.get('chat', False) %}
{% if chat %}

{% set sites = pillar.get('sites', {}) %}
{% for domain, settings in sites.items() %}

/home/web/repo/project/{{ domain }}/live/config:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/repo/project/{{ domain }}

{# Mattermost System Console writes to 'config.json', so don't initialise #}
/home/web/repo/project/{{ domain }}/live/config/config.json.getting-started:
  file:
    - managed
    - source: salt://chat/config.json
    - user: web
    - group: web
    - template: jinja
    - context:
      domain: {{ domain }}
      settings: {{ settings }}
    - require:
      - file: /home/web/repo/project/{{ domain }}/live/config

{# Update 'PushNotificationContents' for mobile phone #}
{# https://chat.kbsoftware.co.uk/kb/pl/66m7o7ki1braibjwuymagqwaqr #}
/home/web/repo/project/{{ domain }}/live/config/config.json:
  file.patch:
    - source: salt://chat/mattermost-config.patch
    - require:
      - file: /home/web/repo/project/{{ domain }}/live/config
{% endfor %} # domain, settings

{% endif %}
