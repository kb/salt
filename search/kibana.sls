{% set kibana = pillar.get('kibana', None) %}
{% if kibana %}

kibana_soft:
  pkg.installed:
    - name: kibana
    - refresh: True
    - require:
      - pkg: elasticsearch_soft

kibana_service:
  service.running:
    - name: kibana
    - enable: True
    - require:
      - pkg: kibana_soft

/etc/kibana/kibana.yml:
  file.managed:
    - source: salt://search/kibana.yml
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      kibana: {{ kibana }}
    - require:
      - pkg: kibana_soft

install_apm:
  pkg.installed:
    - pkgs:
      - apm-server

/etc/apm-server/apm-server.yml:
  file.managed:
    - source: salt://search/apm-server.yml
    - user: apm-server
    - group: apm-server
    - mode: 600
    - template: jinja
    - context:
      kibana: {{ kibana }}
    - require:
      - pkg: kibana_soft

apm_server_service:
  service.running:
    - name: apm-server
    - enable: True
    - require:
      - pkg: install_apm
    - watch:
      - file: /etc/apm-server/apm-server.yml

{% endif %}
