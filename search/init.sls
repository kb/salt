{% set kibana = pillar.get('kibana', False) %}
{% set search = pillar.get('search', None) %}
{% if search %}
{% set server_meta = pillar.get('server_meta', False) %}
{% if server_meta %}
{% set network_hosts = server_meta.get('search_network_hosts', ["127.0.0.1"]) %}
{% else %}
{% set network_hosts = ["127.0.0.1"] %}
{% endif %}

# From
# https://github.com/saltstack-formulas/elasticsearch-logstash-kibana-formula/blob/master/kibana/init.sls
#
# 22/08/2020, Temporary use of the ``kibana`` variable to install the latest
#             version of elasticsearch, kibana and apm for the monitor server.

{% if grains['osmajorrelease'] >= 22  %}
deb [signed-by=/etc/apt/keyrings/elasticsearch-keyring.gpg arch=amd64] https://artifacts.elastic.co/packages/8.x/apt stable main:
{% else %}
deb [signed-by=/etc/apt/keyrings/elasticsearch-keyring.gpg arch=amd64] https://artifacts.elastic.co/packages/6.x/apt stable main:
{% endif %}
  pkgrepo.managed:
    - humanname: Elasticsearch
    - file: /etc/apt/sources.list.d/elasticsearch.list
    - key_url: https://artifacts.elastic.co/GPG-KEY-elasticsearch
    - aptkey: False

elasticsearch_soft:
  pkg.installed:
    - name: elasticsearch
    - refresh: True
    - require:
      - pkg: openjdk

/etc/default/elasticsearch:
  file.managed:
    - source: salt://search/elasticsearch
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - pkg: elasticsearch_soft

/etc/elasticsearch/jvm.options:
  file.managed:
    - source: salt://search/jvm.options
    - user: root
    - template: jinja
    - group: elasticsearch
    - mode: 644
    - context:
      kibana: {{ kibana }}
    - require:
      - pkg: elasticsearch_soft

/etc/elasticsearch/elasticsearch.yml:
  file.managed:
    - source: salt://search/elasticsearch.yml
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      network_hosts: {{ network_hosts }}
    - require:
      - pkg: elasticsearch_soft

elastic_phonetic_plugin:
    cmd.run:
      - name: bin/elasticsearch-plugin install analysis-phonetic
      - cwd: /usr/share/elasticsearch
      - unless: test -d /usr/share/elasticsearch/plugins/analysis-phonetic
      - require:
        - pkg: elasticsearch_soft
        - file: /etc/default/elasticsearch

elastic_service:
  service.running:
    - name: elasticsearch
    - enable: True
    - require:
      - pkg: elasticsearch_soft
      - file: /etc/default/elasticsearch
    - watch:
      - file: /etc/default/elasticsearch
{% endif %}
