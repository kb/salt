{% set r = pillar.get('r', None) %}
{% if r %}

r-base:
  pkgrepo.managed:
    - humanname: R-cran35
    - name: deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/
    - dist: bionic-cran35/
    - file: /etc/apt/sources.list.d/r-cran.list
    - keyid: E084DAB9
    - keyserver: keyserver.ubuntu.com

  pkg.latest:
    - name: r-base
    - refresh: True

libcurl4-openssl-dev:
  pkg.installed

# PJK 08/04/2019 I had to install the following packages manually on the live
# Shiny Server (so comment out until I can find a way for Salt to do it!
#
# note: this takes a very long time to compile
# install_shiny:
#   cmd.run:
#     - name: R -e "install.packages('shiny', repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('shiny')" | grep "\["
#     - require:
#       - pkg: r-base
#
# install_shiny_server:
#   pkg.installed:
#     - sources:
#       - shiny_server: https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.9.923-amd64.deb
#
# install_r_dt:
#   cmd.run:
#     - name: R -e "install.packages('DT', dependencies=TRUE, repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('DT')" | grep "\["
#     - runas: shiny
#     - require:
#       - pkg: r-base
#
# install_r_fasttime:
#   cmd.run:
#     - name: R -e "install.packages('fasttime', dependencies=TRUE, repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('fasttime')" | grep "\["
#     - runas: shiny
#     - require:
#       - pkg: r-base
#
# install_r_plotly:
#   cmd.run:
#     - name: R -e "install.packages('plotly', dependencies=TRUE, repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('plotly')" | grep "\["
#     - runas: shiny
#     - require:
#       - pkg: r-base
#
# install_r_rmarkdown:
#   cmd.run:
#     - name: R -e "install.packages('rmarkdown', repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('rmarkdown')" | grep "\["
#     - runas: shiny
#     - require:
#       - pkg: r-base
#
# # this doesn't work - to install, https://www.pkimber.net/howto/r/index.html
# install_r_jsonlite:
#   cmd.run:
#     - name: R -e "install.packages('jsonlite', dependencies=TRUE, repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('jsonlite')" | grep "\["
#     - runas: shiny
#     - require:
#       - pkg: r-base
#
# install_r_shinydashboard:
#   cmd.run:
#     - name: R -e "install.packages('shinydashboard', repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('shinydashboard')" | grep "\["
#     - runas: shiny
#     - require:
#       - pkg: r-base
#
# # this doesn't work - to install, https://www.pkimber.net/howto/r/index.html
# install_r_tidyverse:
#   cmd.run:
#     - name: R -e "install.packages('tidyverse', repos='https://cran.rstudio.com/')"
#     - unless: R --silent -e "packageVersion('tidyverse')" | grep "\["
#     - runas: shiny
#     - require:
#       - pkg: r-base
#       - pkg: libcurl4-openssl-dev

# We need the following require, but Salt doesn't think 'install_shiny_server'
# is working at the moment, so we will leave it out for now.
#   - require:
#     - pkg: install_shiny_server
/etc/shiny-server/shiny-server.conf:
  file:
    - managed
    - source: salt://r/shiny-server.conf
    - user: root
    - group: root
    - mode: 644
    - template: jinja

{% endif %}
