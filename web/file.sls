{% set alfresco = pillar.get('alfresco', False) %}
{% set backup_config = pillar.get('backup', {}) %}
{% set chat = pillar.get('chat', False) %}
{% set django = pillar.get('django', None) %}
{% set dropbox = pillar.get('dropbox', False) %}
{% set solr = pillar.get('solr', None) %}
{% set devpi = pillar.get('devpi', None) %}
{% set apache = pillar.get('apache', None) %}
{% set php = pillar.get('php', None) %}
{% set static = pillar.get('static', None) %}
{% set workflow = pillar.get('workflow', False) %}
{% set do_token = pillar.get('do_token', False) %}

{# pass an empty parameter #}
{% set empty_dict = {} %}

{% set users = pillar.get('users', {}) %}

{% if alfresco or chat or devpi or django or dropbox or apache or php or static %}
/etc/cron.d/letsencrypt:
  file:
    - managed
    - template: jinja
    - source: salt://web/letsencrypt-cron
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

{% if do_token %}
/etc/letsencrypt/do-token.ini:
  file:
    - managed
    - template: jinja
    - source: salt://web/do-token.ini
    - user: root
    - group: root
    - mode: 600
    - context:
      do_token: {{ do_token }}
{% endif %}

/home/web:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - user: web

/home/web/opt:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - user: web

/usr/local/bin/init-letsencrypt:
  file:
    - managed
    - template: jinja
    - source: salt://web/init-letsencrypt
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/usr/local/bin/parse-log:
  file:
    - managed
    - source: salt://web/parse-log
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/usr/local/bin/htpasswd:
  file:
    - managed
    - source: salt://web/htpasswd
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/usr/local/bin/need-reboot:
  file:
    - managed
    - source: salt://web/need-reboot
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

{% endif %} # devpi or dropbox or apache
{% if chat or django or dropbox %}

{% set sites = pillar.get('sites', {}) %}

/home/web/opt/manage_env.py:
  file:
    - managed
    - source: salt://web/manage_env.py
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - user: web

/usr/local/bin/maintenance-mode:
  file:
    - managed
    - source: salt://web/maintenance-mode
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

{% for domain, settings in sites.items() %}
{% set cron = settings.get('cron', {}) -%}
{% set db_shared = settings.get('db_shared', None) -%}
{% set backup_type = settings.get('backup', "remote") -%}
{# local_backup_weekly only applies if backup_type is "local" #}
{% set local_backup_weekly = settings.get('local_backup_weekly', True) -%}
{% set php_profile = settings.get('php_profile', None) -%}

/home/web/opt/{{ domain }}.sh:
  file:
    - managed
    - source: salt://web/manage.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      domain: {{ domain }}
    - require:
      - file: /home/web/opt
      - user: web

/home/web/repo/files/{{ domain }}/sample-maintenance.html:
  file:
    - managed
    - source: salt://web/sample-maintenance.html
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      domain: {{ domain }}
    - require:
      - file: /home/web/repo/files/{{ domain }}/public
      - user: web

/home/web/repo/files/{{ domain }}/deny-robots.txt:
  file:
    - managed
    - source: salt://web/deny-robots.txt
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      domain: {{ domain }}
    - require:
      - file: /home/web/repo/files/{{ domain }}/public
      - user: web

{% if not db_shared %}
/home/web/opt/backup.{{ domain }}.sh:
  file:
    - managed
    - source: salt://web/backup.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      backup_config: {{ backup_config }}
      domain: {{ domain }}
      backup_type: {{ backup_type }}
      local_backup_weekly: {{ local_backup_weekly }}
    - require:
      - file: /home/web/opt
      - user: web

{% if workflow %}
/home/web/opt/backup.workflow.{{ domain }}.sh:
  file:
    - managed
    - source: salt://web/backup_workflow.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      backup_config: {{ backup_config }}
      domain: {{ domain }}
      backup_type: {{ backup_type }}
      local_backup_weekly: {{ local_backup_weekly }}
    - require:
      - file: /home/web/opt
      - user: web
{% endif %} # workflow
{% endif %} # db_shared

{# create cron.d file even if it is empty... #}
{# or we won't be able to remove items from it #}
/etc/cron.d/{{ domain|replace('.', '_')|replace('-', '_') }}:
  file:
    - managed
    - source: salt://web/cron_for_site
    - user: root
    - group: root
    - mode: 755
    - template: jinja
    - context:
      cron: {{ cron }}
      cron_user: "web"
      alfresco: {{ empty_dict }}
      run_backup_script: {{ django }}
      django: {{ django }}
      dropbox: {{ empty_dict }}
      domain: {{ domain }}
      php_profile: {{ php_profile }}
      workflow: {{ workflow }}
      backup_type: {{ backup_type }}

{% endfor %} # domain, settings

{% endif %} # chat or django or dropbox


{# create cron.d file for alfresco if required #}
/etc/cron.d/alfresco:
{% if alfresco %}
  file:
    - managed
    - source: salt://web/cron_for_site
    - user: root
    - group: root
    - mode: 755
    - template: jinja
    - context:
      cron: {{ empty_dict }}
      cron_user: "web"
      alfresco: {{ alfresco }}
      run_backup_script: {{ empty_dict }}
      django: {{ empty_dict }}
      dropbox: {{ empty_dict }}
      php_profile: {{ empty_dict }}
      backup_type: "remote"
{% else %}
  file.absent
{% endif %}

{# create cron.d file for dropbox if required #}
/etc/cron.d/dropbox:
{% if dropbox %}
  file:
    - managed
    - source: salt://web/cron_for_site
    - user: root
    - group: root
    - mode: 755
    - template: jinja
    - context:
      cron: {{ empty_dict }}
      cron_user: "web"
      alfresco: {{ empty_dict }}
      run_backup_script: {{ empty_dict }}
      django: {{ empty_dict }}
      dropbox: {{ dropbox }}
      php_profile: {{ empty_dict }}
      backup_type: "remote"
{% else %}
  file.absent
{% endif %}

{% if backup_config|length %}
{% if dropbox %}
{% for account in dropbox.accounts %}
/home/web/opt/backup_dropbox_{{ account }}.sh:
  file:
    - managed
    - source: salt://web/backup_dropbox.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      backup_config: {{ backup_config }}
      dropbox_account: {{ account }}
    - require:
      - file: /home/web/opt
      - user: web
{% endfor %}
{% endif %} # dropbox
{% endif %} # backup_config

{% if php %}
/usr/local/bin/restore.sh:
  file:
    - managed
    - source: salt://web/drupal/restore.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
    - require:
      - file: /home/web/opt
      - user: web
/usr/local/bin/restore_db.sh:
  file:
    - managed
    - source: salt://web/drupal/restore_db.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
    - require:
      - file: /home/web/opt
      - user: web

{% set sites = pillar.get('sites', {}) %}
{% for domain, settings in sites.items() %}

{% set cron = settings.get('cron', {}) -%}
{% set db_pass = settings.get('db_pass', {}) -%}
{% set db_user = settings.get('db_user', False) -%}
{% set php_profile = settings.get('php_profile', None) %}
{% set backup_type = settings.get('backup', "remote") -%}
{% set local_backup_weekly = settings.get('local_backup_weekly', True) -%}
{% if php_profile == "drupal" %}
{% set theme_name = settings.get('theme_name') -%}
/usr/local/bin/deploy-drupal-{{ domain }}.sh:
  file:
    - managed
    - source: salt://web/drupal/deploy-drupal.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      domain: {{ domain }}
      theme_name: {{ theme_name }}
    - require:
      - file: /home/web/opt
      - user: web
/home/web/opt/backup.{{ domain }}.sh:
  file:
    - managed
    - source: salt://web/backup_php.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - context:
      backup_config: {{ backup_config }}
      domain: {{ domain }}
      db_pass: {{ db_pass }}
      db_user: {{ db_user }}
      backup_type: {{ backup_type }}
      local_backup_weekly: {{ local_backup_weekly }}
    - require:
      - file: /home/web/opt
      - user: web
{% elif php_profile == "wordpress" %}
wp-cli-{{ domain }}:
  cmd.run:
    - name: |
        cd /tmp
        curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
        mv /tmp/wp-cli.phar /home/web/opt/wp
        chmod a+x /home/web/opt/wp
        chown web:web /home/web/opt/wp
    - cwd: /tmp
    - shell: /bin/bash
    - timeout: 300
    - creates: /home/web/opt/wp
/home/web/opt/backup.{{ domain }}.sh:
  file:
    - managed
    - source: salt://web/backup_wordpress.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - context:
      backup_config: {{ backup_config }}
      domain: {{ domain }}
      db_pass: {{ db_pass }}
      db_user: {{ db_user }}
      backup_type: {{ backup_type }}
      local_backup_weekly: {{ local_backup_weekly }}
    - require:
      - file: /home/web/opt
      - user: web
{% else %}
/home/web/opt/backup.{{ domain }}.sh:
  file:
    - managed
    - source: salt://web/backup_php.sh
    - user: web
    - group: web
    - mode: 755
    - template: jinja
    - makedirs: True
    - context:
      backup_config: {{ backup_config }}
      local_backup_weekly: {{ local_backup_weekly }}
      domain: {{ domain }}
      db_pass: {{ db_pass }}
    - require:
      - file: /home/web/opt
      - user: web
{% endif %}

{# create cron.d file even if it is empty... #}
{# or we won't be able to remove items from it #}
/etc/cron.d/{{ domain|replace('.', '_') }}:
  file:
    - managed
    - source: salt://web/cron_for_site
    - user: root
    - group: root
    - mode: 755
    - template: jinja
    - context:
      cron: {{ cron }}
      {% if php_profile == "drupal" or php_profile == "custom" %}
      cron_user : "root"
      {% else %}
      cron_user : "web"
      {% endif %}
      alfresco: {{ empty_dict }}
      run_backup_script: True
      django: {{ empty_dict }}
      dropbox: {{ empty_dict }}
      domain: {{ domain }}
      php_profile: {{ php_profile }}
      workflow: {{ empty_dict }}
      backup_type: {{ backup_type }}

{% endfor %} # domain, settings
{% endif %} # php
