#!/bin/bash

if [ "$4" == "" ]
then
    echo -e "\nUsage:\n\t $(basename $0) <tar archive> <new domain> <theme name> <drupal version>\n"
    echo -e "\nWhere:\n\t<tar archive> is a files backup of a drupal site"
    echo -e "\t<new domain> is the new domain (should be deployed using salt to get the correct structure)"
    echo -e "\t<theme name> is the current theme on the site with an images folder in the theme directory"
    echo -e "\t<drupal version> is the drupal version to deploy using semantic versioning e.g. 9.5.09\n"
    exit 1
fi

TAR_FILE=$1

if [ ! -f "${TAR_FILE}" ];then
    echo -e "\nThe tar archive '${TAR_FILE}' does not exist\n"
    exit 1
fi

echo "This script uses sudo you may be prompted for your password"

SITE_NAME="$2"
THEME_NAME="$3"
VERSION="${4//./_}"
SITE_PATH="/home/web/repo/project/${SITE_NAME}"
TAR_PATH=$(realpath ${TAR_FILE})
DIR_NAME="$(date +"${VERSION}__%Y%m%d_%H%M%S_${USER}")"
DIR_PATH="${SITE_PATH}/deploy/${DIR_NAME}"

echo "Creating deploy directory..."
sudo mkdir -p ${DIR_PATH}

cd ${DIR_PATH}


if [ `pwd` != "${DIR_PATH}" ]
then
    echo "Failed to create and change to deploy directory (${DIR_PATH})"
    echo ""
    exit 1
fi

echo "Deploy directory: `pwd`"
echo "Extracting files..."

sudo tar -xzf ${TAR_PATH}
sudo chown -R web:web .
sudo chmod -R a+rX .
sudo chown -R www-data:www-data sites/default/files

cd ${SITE_PATH}

echo "Creating live link in `pwd`"
if [[ -f 'live' || -d live ]]; then
    sudo mv live was-live-until-$(date +%Y%m%d_%H%M%S)
fi

sudo ln -s "deploy/${DIR_NAME}/web" live

sudo rm live/sites/default/images
sudo ln -s ${SITE_PATH}/live/sites/default/themes/${THEME_NAME}/images live/sites/default/images

exit 0

