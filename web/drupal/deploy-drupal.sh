#!/bin/bash
# This script will deploy a new version of drupal and any modules required
# using a predefined composer.json and assumes you have an existing copy of
# your site already on this server.  To create a new server restore the site
# from backup using the restore.sh and restore-db.sh scripts.

DOMAIN={{ domain }}
THEME_NAME={{ theme_name }}
PROJECT_PATH=/home/web/repo/project/${DOMAIN}

function print_usage {
  echo -e "\nUsage:\n\t$(basename $0) <version e.g. 10.1.01>\n"
  exit 2
}

if [ "$EUID" -eq 7500 ]; then
    print_usage
    echo -e "\nCannot run this script as web as sudo privilege is required"
    exit 2
fi

if [ "$1" = "" ];then 
    print_usage
fi

MAJOR_VERSION=${1%%.*}
VERSION_LENGTH=7
if [ ${MAJOR_VERSION} -lt 10 ];then
    VERSION_LENGTH=6
fi

if [ $(expr length $1) -lt ${VERSION_LENGTH} ]; then
    print_usage
fi

VERSION=$1
REAL_USER=$(who am i | awk '{ print $1 }')

DEPLOY_PATH=${PROJECT_PATH}/deploy/${VERSION//./_}__$(date +%Y%m%d_%H%M%S_${REAL_USER})

REAL_LIVE_PATH=$(readlink -f ${PROJECT_PATH}/live)
PREV_DEPLOY_PATH=$(dirname ${REAL_LIVE_PATH})

sudo -u web mkdir $DEPLOY_PATH

cd $DEPLOY_PATH

if [ "$(pwd)" != "$DEPLOY_PATH" ]; then
  echo -e "\nFailed to create and change to $DEPLOY_PATH\n"
  exit
fi

sudo -u web cp $PREV_DEPLOY_PATH/composer.json .

echo -e "\nEdit composer.json before deploy (Y/n) \c"
read OK

if [ "$OK" != "N" -a "$OK" != "n" ]; then
  sudo -u web ${EDITOR:-vim} composer.json

  echo -e "\nContinue with install (Y/n) \c"
  read OK

  if [ "$OK" = "N" -o "$OK" = "n" ]; then
    exit 
  fi
fi

sudo -u web composer install

sudo -u web mkdir web/.well-known
sudo chown -R www-data:www-data web/.well-known


sudo -u web cp ${PREV_DEPLOY_PATH}/web/sites/default/settings.php web/sites/default/
sudo -u web cp -R  ${PREV_DEPLOY_PATH}/web/sites/default/themes/ web/sites/default/
sudo -u web ln -s ${DEPLOY_PATH}/web/sites/default/themes/${THEME_NAME}/images/ web/sites/default/

sudo -u web cp -R  ${PREV_DEPLOY_PATH}/web/sites/default/files/ web/sites/default/
sudo chown -R www-data:www-data web/sites/default/files


if [ -d 'web/sites/default/themes/' -a -d 'sites/default/files/' -a -d 'web/.well-known' -a -f 'web/sites/default/settings.php' ];then
    echo "Copied themes and files directories to new deployment"
    echo "Copied settings.php to new deployment"
    echo "Created .well-known directory for letsencrypt"
    if [ $(stat --format '%U' web/sites/default/files) = 'www-data' -a $(stat --format '%U' web/.well-known) = 'www-data' ]; then
        echo "Files and .well-known directories owned by 'www-data'"
    fi
fi

cd $PROJECT_PATH
echo -e "\nNew version installed in directory:\n\n\t ${DEPLOY_PATH}\n"
echo -e "DO NOT make this site live if changes are required to settings.php\n"
echo -e "\rMake Site live (y/N) \c"

read LIVE_OK

if [ "${LIVE_OK}" = "y" -o "${LIVE_OK}" = "Y" ];then
    sudo rm live
    sudo -u web ln -s ${DEPLOY_PATH}/web/ live

    REAL_LIVE_PATH=$(readlink -f live)
fi

if [ "$REAL_LIVE_PATH" = "$DEPLOY_PATH/web" ];then
    echo -e "\nThe new version of the site is live, now do the following:"
else
    echo -e "\n\n*** The new version is NOT live. ***\n"
    echo -e "Execute the following commands to make it live:\n"
    echo -e "\tcd ${PROJECT_PATH}"
    echo -e "\tln -s ${DEPLOY_PATH}/web/ live"
    echo -e "\nOnce the new version of the site is live do the following:"
fi

echo -e "\n\tBackup the database using the command:"
echo -e "\n\t\tsudo mysqldump ${DOMAIN//./_} > ~/dump-$(date +%Y-%m-%d)\n"
echo -e "\n\tLog on to the site as the admin user"
echo -e "\tPut the site in maintenance mode"
echo -e "\tThen in the same browser container browse to\n"
echo -e "\t\thttps://${DOMAIN}/update.php"
echo -e "\n\tand follow the on screen instructions"
echo -e "\tThen restore normal site mode"
