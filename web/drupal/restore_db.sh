#! /bin/bash

if [ "$3" == "" ]
then
    echo -e "\nUsage : \n\t$0 <mysql backup file> <new domain> <password>\n"
    exit 1
fi

BACKUP_SCRIPT="$(realpath $1)"

DB_NAME="${2//./_}"
DB_USER="$DB_NAME"
DB_PASS="$3"

if [ -f $BACKUP_SCRIPT ]
then
    echo -e "\nThis script will restore the database ($DB_NAME) using the backup file:"
    echo -e "\n\t$BACKUP_SCRIPT"
    echo -e "\nContinue (y/N) \c"
    read input

    echo ""

    if [ "$input" = "Y" -o "$input" = "y" ]
    then
        echo "Restoring the database..."
        echo "if prompted please enter your password to perform sudo"
        echo "drop database if exists $DB_NAME; create database $DB_NAME; create user if not exists '$DB_USER'@'localhost' identified by '$DB_PASS'; grant all on $DB_NAME.* to '$DB_USER'@'localhost';" | sudo mysql
        echo "use $DB_NAME; source $BACKUP_SCRIPT;" | sudo mysql
    fi
else
    echo "Backup script \"$BACKUP_SCRIPT\" does not exist"
    exit 2
fi
