#!/bin/bash
#
# script to backup {{ domain }} database and files
#
#
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u
{% set rsync = gpg['rsync'] -%}
DOMAIN={{ domain }}
DB_NAME=${DOMAIN//./_}
DB_USER=typingace
DB_PASS={{ db_pass }}
PROJECT_DIR=/home/web/repo/project/${DOMAIN}
DB_BACKUP_DIR=/home/web/repo/backup/${DOMAIN}
FILES_DIRECTORY=`readlink -f ${PROJECT_DIR}/live`
EXCLUDE_DIR=${FILES_DIRECTORY}/TypingAce-Intranet/typingace/twlibs/cache/
export RESTIC_PASSWORD="{{ rsync['pass'] }}"

echo "------------------------------------------------------------------------"
echo "restic"

# dump database
echo "------------------------------------------------------------------------"
DUMP_FILE=${DB_BACKUP_DIR}/$(date +"%Y%m%d_%H%M").sql
echo "dump database: ${DUMP_FILE}"
mysqldump --host=localhost --user=${DB_USER} --password=${DB_PASS} ${DB_NAME} > ${DUMP_FILE}

echo "------------------------------------------------------------------------"
echo "database - backup"
/home/web/opt/restic -r sftp:{{ rsync['user'] }}@{{ rsync['server'] }}:restic/${DOMAIN}/backup backup ${DB_BACKUP_DIR}
echo "------------------------------------------------------------------------"
echo "database - remove dump: $DUMP_FILE"
rm $DUMP_FILE
echo "------------------------------------------------------------------------"
echo "database - forget/prune"
/home/web/opt/restic -r sftp:{{ rsync['user'] }}@{{ rsync['server'] }}:restic/${DOMAIN}/backup forget --keep-last 10 --keep-hourly 20 --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 10
echo "------------------------------------------------------------------------"
echo "database - check"
/home/web/opt/restic -r sftp:{{ rsync['user'] }}@{{ rsync['server'] }}:restic/${DOMAIN}/backup check


echo "------------------------------------------------------------------------"
echo "files - backup"
/home/web/opt/restic -r sftp:{{ rsync['user'] }}@{{ rsync['server'] }}:restic/${DOMAIN}/files backup ${FILES_DIRECTORY} --exclude ${EXCLUDE_DIR}
echo "------------------------------------------------------------------------"
echo "files - forget"
/home/web/opt/restic -r sftp:{{ rsync['user'] }}@{{ rsync['server'] }}:restic/${DOMAIN}/files forget --keep-last 10 --keep-hourly 20 --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 10
echo "------------------------------------------------------------------------"
echo "files - check"
/home/web/opt/restic -r sftp:{{ rsync['user'] }}@{{ rsync['server'] }}:restic/${DOMAIN}/files check

unset RESTIC_PASSWORD
echo "------------------------------------------------------------------------"
