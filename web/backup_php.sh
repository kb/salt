#!/bin/bash
#
# script to backup {{ domain }} database and files
#
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u
PROJECT_DIR=/home/web/repo/project/{{ domain }}
DB_BACKUP_DIR=/home/web/repo/backup/{{ domain }}
FILES_DIRECTORY=`readlink -f ${PROJECT_DIR}/live`

{% set db_name = domain|replace('.', '_')|replace('-', '_') -%}
# dump database
{% if backup_type is defined and backup_type == "remote" -%}
DUMP_FILE=${DB_BACKUP_DIR}/$(date +"%Y%m%d_%H%M").sql.bz2
export RESTIC_PASSWORD="{{ backup_config['pass'] }}"
echo "------------------------------------------------------------------------"
{% else -%}
DUMP_FILE=${DB_BACKUP_DIR}}/{{ domain }}.{% if local_backup_weekly %}$(date +"%A"){%else %}$(date +"%Y%m%d_%H%M"){% endif %}.sql.bz
echo "----------- Backup not configured - just dump database -----------------"
{% endif -%} {# backup_type == "remote" #}
echo "dump database: ${DUMP_FILE}"
mysqldump --no-tablespaces {{ db_name }} | nice -n 19 bzip2 > ${DUMP_FILE}

{% if backup_type is defined and backup_type =="remote" -%}
echo "------------------------------------------------------------------------"
echo "database - backup"
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup backup ${DB_BACKUP_DIR}
echo "------------------------------------------------------------------------"
echo "database - remove dump: $DUMP_FILE"
rm $DUMP_FILE
echo "------------------------------------------------------------------------"
echo "database - forget and prune"
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup forget --keep-last 2 --keep-hourly 1 --keep-daily 1 --keep-weekly 5 --keep-monthly 6
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup prune
echo "------------------------------------------------------------------------"
echo "database - check"
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup check

echo "------------------------------------------------------------------------"
echo "files - backup"
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files backup ${FILES_DIRECTORY}
echo "------------------------------------------------------------------------"
echo "files - forget and prune"
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files forget --keep-last 2 --keep-hourly 1 --keep-daily 1 --keep-weekly 5 --keep-monthly 6
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files prune
echo "------------------------------------------------------------------------"
echo "files - check"
sudo --preserve-env=RESTIC_PASSWORD -u web restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files check
unset RESTIC_PASSWORD
{% endif %}
echo "------------------------------------------------------------------------"
