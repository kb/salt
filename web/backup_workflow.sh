#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

{% if backup_type == "remote" -%}
DUMP_FILE=/home/web/repo/backup/{{ domain }}/workflow_$(date +"%Y%m%d_%H%M").sql
export RESTIC_PASSWORD="{{ backup_config['pass'] }}"
echo "------------------------------------------------------------------------"
{% else -%} {# backup_type #}
DUMP_FILE=/home/web/repo/backup/{{ domain }}/{{ domain }}_workflow.{% if local_backup_weekly %}$(date +"%A"){%else %}$(date +"%Y%m%d_%H%M"){% endif %}.sql
{% endif -%} {# backup_type #}

echo "------------------------------------------------------------------------"
echo "database - dump: $DUMP_FILE"
pg_dump -U postgres {{ domain|replace('.', '_')|replace('-', '_') }}_workflow -f $DUMP_FILE
echo "------------------------------------------------------------------------"

{% if backup_type == "remote" -%}
echo "database - backup"
/home/web/opt/restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/workflow backup /home/web/repo/backup/{{ domain }}
echo "------------------------------------------------------------------------"
echo "database - remove dump: $DUMP_FILE"
rm $DUMP_FILE

# 18/10/2022, Do the database backup before doing the forget (as it seems that is the only part of the backup that requires a lock)
#
echo "------------------------------------------------------------------------"
echo "database - forget/prune"
/home/web/opt/restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/workflow forget --keep-last 10 --keep-hourly 20 --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 10
echo "------------------------------------------------------------------------"
echo "database - check"
/home/web/opt/restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/workflow check

unset RESTIC_PASSWORD
echo "------------------------------------------------------------------------"
{% else -%}
echo "Backup not configured - just dump database -----------------------------"
{% endif -%} {# backup_type #}
