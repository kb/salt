#!/bin/bash
#
# script to backup {{ domain }} database and files for wordpress
#
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

{% if backup_type == "remote" -%}
DUMP_FILE=/home/web/repo/backup/{{ domain }}/$(date +"%Y%m%d_%H%M").sql.bz2
export RESTIC_PASSWORD="{{ backup_config['pass'] }}"
{% else -%}
DUMP_FILE=/home/web/repo/backup/{{ domain }}/{{ domain }}.{% if local_backup_weekly %}$(date +"%A"){%else %}$(date +"%Y%m%d_%H%M"){% endif %}.sql.bz2
{% endif -%}
{% set db_name = domain|replace('.', '_')|replace('-', '_') -%}
{% if not db_user -%}
{% set db_user = db_name %}
{% endif -%}

echo "------------------------------------------------------------------------"
echo "database - dump: $DUMP_FILE"
mysqldump --user={{ db_user }} --password={{ db_pass }} --no-tablespaces {{ db_name }} | nice -n 19 bzip2 > ${DUMP_FILE}
echo "------------------------------------------------------------------------"

{% if backup_type == "remote" -%}
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup backup /home/web/repo/backup/{{ domain }}
echo "------------------------------------------------------------------------"
echo "database - remove dump: $DUMP_FILE"
rm $DUMP_FILE
echo "------------------------------------------------------------------------"

echo "------------------------------------------------------------------------"
echo "files - backup"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files backup /home/web/repo/project/{{ domain }}/live
echo "------------------------------------------------------------------------"
echo "backup - forget and prune"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup forget --keep-last 10 --keep-hourly 20 --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 10
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup prune
echo "------------------------------------------------------------------------"
echo "backup - check"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup check

echo "files - forget and prune"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files forget --keep-last 10 --keep-hourly 20 --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 10
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files prune
echo "------------------------------------------------------------------------"
echo "files - check"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files check

unset RESTIC_PASSWORD
echo "------------------------------------------------------------------------"

{% endif -%} {# backup_type == "remote" #}
