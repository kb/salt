{% set alfresco = pillar.get('alfresco', False) %}
{% set chat = pillar.get('chat', False) %}
{% set devpi = pillar.get('devpi', None) %}
{% set dropbox = pillar.get('dropbox', None) %}
{% set django = pillar.get('django', None) %}
{% set apache_php = pillar.get('apache_php', None) %}
{% set php = pillar.get('php', None) %}
{% set static = pillar.get('static', None) %}
{# https://stackoverflow.com/questions/3727045/set-variable-in-jinja #}
{% set ns = namespace(is_wordpress=False) %}

{% if alfresco or apache_php or chat or devpi or django or dropbox or php or static %}

{% set sites = pillar.get('sites', {}) %}

{# Create repo folder (required for other bits) #}
/home/web/repo:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - user: web

/home/web/repo/backup:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - file: /home/web/repo

/home/web/repo/files:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - file: /home/web/repo

{% if dropbox %}
/home/web/repo/files/dropbox:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - file: /home/web/repo/files

{% for account in dropbox.accounts -%}
/home/web/repo/files/dropbox/{{ account }}:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - file: /home/web/repo/files/dropbox
{% endfor -%}
{% endif %} {# dropbox #}

/home/web/repo/project:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - file: /home/web/repo

/home/web/repo/temp:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: False
    - require:
      - file: /home/web/repo

{% for domain, settings in sites.items() %}

/home/web/repo/backup/{{ domain }}:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/repo/backup

{# 'files/site/public' folder is for public uploads #}
/home/web/repo/files/{{ domain }}/public:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/repo/files

{# 'files/site/private' folder is for private attachments #}
/home/web/repo/files/{{ domain }}/private:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/repo/files

{# 'project' folder is for the code #}
/home/web/repo/project/{{ domain }}:
  file.directory:
    - user: web
    - group: web
    - mode: 755
    - makedirs: True
    - require:
      - file: /home/web/repo/project

{% set php_profile = settings.get('php_profile', None) %}

{% if php_profile == 'nextcloud' or php_profile == 'wordpress' -%}
/home/web/repo/project/{{ domain }}/live:
  file.directory:
    - user: www-data
    - group: www-data
    - dir_mode: 775
    - file_mode: 664
    - recurse:
      - user
      - group
      - mode
    - require:
      - file: /home/web/repo/project/{{ domain }}

{# the state above will switch this to 644 - then we switch it back #}
/home/web/repo/project/{{ domain }}/live/wp-config.php:
  file.managed:
    - user: www-data
    - group: www-data
    - mode: 444
    - replace: False
    - require:
      - file: /home/web/repo/project/{{ domain }}
{% endif -%}

{% if php_profile == 'wordpress' %}
{% set ns.is_wordpress = True %}
{% endif %}

{% endfor %} # domain, settings

{% if ns.is_wordpress == True %}
/var/log/wordpress:
  file.directory:
    - user: www-data
    - group: www-data
    - mode: 755
    - makedirs: True
{% endif %}

{% endif %} # alfresco/apache_php/chat/devpi/django/dropbox/monitor/php
