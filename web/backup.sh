#!/bin/bash
#
# Script to back up a postgresql database and files directory
#
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

{% if backup_type == "remote" -%}
DUMP_FILE=/home/web/repo/backup/{{ domain }}/$(date +"%Y%m%d_%H%M").sql
export RESTIC_PASSWORD="{{ backup_config['pass'] }}"
echo "------------------------------------------------------------------------"
{% else -%} {# backup_type #}
DUMP_FILE=/home/web/repo/backup/{{ domain }}/{{ domain }}.{% if local_backup_weekly %}$(date +"%A"){%else %}$(date +"%Y%m%d_%H%M"){% endif %}.sql
{% endif -%} {# backup_type #}

echo "------------------------------------------------------------------------"
echo "database - dump: $DUMP_FILE"
pg_dump -U postgres {{ domain|replace('.', '_')|replace('-', '_') }} -f $DUMP_FILE
echo "------------------------------------------------------------------------"

{% if backup_type == "remote" -%}
echo "database - backup"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup backup /home/web/repo/backup/{{ domain }}
echo "------------------------------------------------------------------------"
echo "database - remove dump: $DUMP_FILE"
rm $DUMP_FILE

echo "------------------------------------------------------------------------"
echo "files - backup"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files backup /home/web/repo/files/{{ domain }}

# 18/10/2022, Do both the database and files backup before doing the forget (as it seems that is the only part of the backup that requires a lock)

echo "------------------------------------------------------------------------"
echo "database - forget and prune"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup forget --keep-last 10 --keep-hourly 20 --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 10
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup prune
echo "------------------------------------------------------------------------"
echo "database - check"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/backup check

echo "------------------------------------------------------------------------"
echo "files - forget and prune"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files forget --keep-last 10 --keep-hourly 20 --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 10
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files prune
echo "------------------------------------------------------------------------"
echo "files - check"
restic -r sftp:{{ backup_config['user'] }}@{{ backup_config['server'] }}:restic/{{ domain }}/files check

unset RESTIC_PASSWORD
echo "------------------------------------------------------------------------"
{% else -%} {# backup_type #}
echo "Backup not configured - just dump database -----------------------------"
{% endif -%} {# backup_type #}
