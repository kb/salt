{% set alfresco = pillar.get('alfresco', None) %}
{% set development = pillar.get('development', None) -%}
{% set django = pillar.get('django', None) %}
{% set dropbox = pillar.get('dropbox', None) %}
{% set php = pillar.get('php', None) %}
{% set apache = pillar.get('apache', None) %}

{% set devpi = pillar.get('devpi', None) %}
{% set sites = pillar.get('sites', {}) %}

{% set letsencrypt = False %}
{% for domain, settings in sites.items() %}
{% if not letsencrypt and settings.get("letsencrypt", False) == "do-dns" %}
{% set letsencrypt = "do-dns" %}
python3-certbot-dns-digitalocean:
  pkg.installed
{% endif %}
{% endfor %}

atop:
  pkg.installed

build-essential:
  pkg.installed

git:
  pkg.installed

unzip:
  pkg.installed

{% if alfresco or django or devpi or apache %}

python3:
  pkg.installed

python3-dev:
  pkg.installed:
    - require:
      - pkg: build-essential

{% if grains['osmajorrelease'] < 20 %}
python-dev:
  pkg.installed:
    - require:
      - pkg: build-essential
{% endif %}

python3-virtualenv:
  pkg.installed

{% endif %} # django or devpi or apache

{% if alfresco or django or devpi or apache or php %}

python3-venv:
  pkg.installed

{% endif %} # django or devpi or apache or php

{% if development or django or php %}

restic:
  pkg.installed

{% endif %} # development or django or php

certbot:
  pkg.installed

{% if django %}

{% if grains['osmajorrelease'] < 22 -%}
{# for pillow #}
libjpeg62-dev:
  pkg.installed
{% else %}
libjpeg-dev:
  pkg.installed
libwebp-dev:
  pkg.installed
{% endif %}

{# for pillow #}
zlib1g-dev:
  pkg.installed

{# for pillow #}
libfreetype-dev:
  pkg.installed

{# for element tree #}
libxml2-dev:
  pkg.installed

{# for element tree #}
libxslt1-dev:
  pkg.installed

{% endif %} # django

{% if dropbox %}

{# copied from https://www.dropbox.com/install?os=lnx #}
dropboxd:
  cmd.run:
    - unless: test -d /home/web/.dropbox-dist
    - cwd: /home/web
    {% if grains['cpuarch'] == 'i686' %}
    - name: 'wget -O - "https://www.dropbox.com/download?plat=lnx.x86" | tar xzf -'
    {% elif grains['cpuarch'] == 'x86_64' %}
    - name: 'wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -'
    {% else %}
    I think 'cpuarch' can only be 'x86' or 'x86_64'
    {% endif %}
    - runas: web

{% endif %} # dropbox
