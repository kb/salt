Salt
****

The documentation for this project can be found at:
https://www.kbsoftware.co.uk/docs/

watch_ftp_folder
================

To test this script::

  virtualenv --python=python3 venv-salt
  source venv-salt/bin/activate

  pip install -r ftp/requirements.txt

Set the following environment variables::

  set -x API_PASSWORD "letmein"
  set -x API_USERNAME "maria.anders"
  set -x API_URL "http://localhost:8000"
  set -x API_PATH "/connect/watcher/"
  set -x FTP_WATCH_FOLDER "/home/patrick/repo/temp/watch/"

Run the script::

  python ftp/watch_ftp_folder.py

To test, copy files into the folder e.g::

  cp ~/Documents/image/profile.jpg ~/repo/temp/watch/
