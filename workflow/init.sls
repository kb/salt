{% set libreoffice_headless = pillar.get('libreoffice_headless', None) -%}
{% if libreoffice_headless -%}
libreoffice:
  pkg:
    - installed
    - install_recommends: False

poppler-utils:
  pkg:
    - installed
    - install_recommends: False
{% endif %}

{% set workflow = pillar.get('workflow', None) %}
{% if workflow %}
{# java and tomcat installed using 'java/init.sls' #}

/usr/share/tomcat9/lib/postgresql-42.5.0.jar:
  file.managed:
    - source: salt://workflow/postgresql-42.5.0.jar
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: tomcat9

/usr/share/tomcat9/lib/postgresql-42.2.9.jar:
  file.absent

{% set postgres_settings = pillar.get('postgres_settings', {}) %}
{% set sites = pillar.get('sites', {}) %}
{% for domain, settings in sites.items() %}
{% if settings.get('workflow', None) -%}

{# 11/10/2019 There is now a **maximum** of one workflow engine per server. #}
{#            If more than one site has workflow enabled, then only one     #}
{#            will be setup correctly.                                      #}

/var/lib/tomcat9/webapps/flowable-rest.war:
  file.managed:
{% if grains['osmajorrelease'] < 22 %}
    - source: salt://workflow/flowable-rest-6.5.war
{% else %}
    - source: salt://workflow/flowable-rest-6.8.war
{% endif %}
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: tomcat9

/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/classes/flowable-default.properties:
  file.managed:
    - source: salt://workflow/flowable-default.properties.rest
    - user: tomcat
    - group: tomcat
    - mode: 644
    - template: jinja
    - context:
      domain: {{ domain }}
      postgres_settings: {{ postgres_settings }}
      settings: {{ settings }}
    - require:
      - pkg: tomcat9


{% if grains['osmajorrelease'] < 22 %}
{# https://nvd.nist.gov/vuln/detail/CVE-2021-44228 #}
{# https://www.kbsoftware.co.uk/crm/ticket/5960/ #}
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-api-2.12.1.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-api-2.15.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-api-2.16.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-api-2.17.0.jar:
  file.absent

/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-core-2.12.1.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-core-2.15.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-core-2.16.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-core-2.17.0.jar:
  file.absent

/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-jul-2.12.1.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-jul-2.15.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-jul-2.16.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-jul-2.17.0.jar:
  file.absent

/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-slf4j-impl-2.12.1.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-slf4j-impl-2.15.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-slf4j-impl-2.16.0.jar:
  file.absent
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-slf4j-impl-2.17.0.jar:
  file.absent

{# https://nvd.nist.gov/vuln/detail/CVE-2021-44228 #}
{# https://www.kbsoftware.co.uk/crm/ticket/5960/ #}
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-api-2.17.1.jar:
  file.managed:
    - source: salt://workflow/log4j-api-2.17.1.jar
    - user: tomcat
    - group: tomcat
    - mode: 640
    - require:
      - pkg: tomcat9

{# https://nvd.nist.gov/vuln/detail/CVE-2021-44228 #}
{# https://www.kbsoftware.co.uk/crm/ticket/5960/ #}
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-core-2.17.1.jar:
  file.managed:
    - source: salt://workflow/log4j-core-2.17.1.jar
    - user: tomcat
    - group: tomcat
    - mode: 640
    - require:
      - pkg: tomcat9

{# https://nvd.nist.gov/vuln/detail/CVE-2021-44228 #}
{# https://www.kbsoftware.co.uk/crm/ticket/5960/ #}
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-jul-2.17.1.jar:
  file.managed:
    - source: salt://workflow/log4j-jul-2.17.1.jar
    - user: tomcat
    - group: tomcat
    - mode: 640
    - require:
      - pkg: tomcat9

{# https://nvd.nist.gov/vuln/detail/CVE-2021-44228 #}
{# https://www.kbsoftware.co.uk/crm/ticket/5960/ #}
/var/lib/tomcat9/webapps/flowable-rest/WEB-INF/lib/log4j-slf4j-impl-2.17.1.jar:
  file.managed:
    - source: salt://workflow/log4j-slf4j-impl-2.17.1.jar
    - user: tomcat
    - group: tomcat
    - mode: 640
    - require:
      - pkg: tomcat9
{% endif %} {# osmajorrelease < 22 #}

{% if settings.get('workflow_tools', None) -%}

{% if grains['osmajorrelease'] < 22 %}

/var/lib/tomcat9/webapps/flowable-idm.war:
  file.managed:
    - source: salt://workflow/flowable-idm-6.5.war
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: tomcat9

/var/lib/tomcat9/webapps/flowable-modeler.war:
  file.managed:
    - source: salt://workflow/flowable-modeler-6.5.war
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: tomcat9

/var/lib/tomcat9/webapps/flowable-idm/WEB-INF/classes/flowable-default.properties:
  file.managed:
    - source: salt://workflow/flowable-default.properties.idm
    - user: tomcat
    - group: tomcat
    - mode: 644
    - template: jinja
    - context:
      domain: {{ domain }}
      settings: {{ settings }}
    - require:
      - pkg: tomcat9

/var/lib/tomcat9/webapps/flowable-modeler/WEB-INF/classes/flowable-default.properties:
  file.managed:
    - source: salt://workflow/flowable-default.properties.modeler
    - user: tomcat
    - group: tomcat
    - mode: 644
    - template: jinja
    - context:
      domain: {{ domain }}
      settings: {{ settings }}
    - require:
      - pkg: tomcat9

{% else %} {# osmajorrelease < 22 #}

/var/lib/tomcat9/webapps/flowable-ui.war:
  file.managed:
    - source: salt://workflow/flowable-ui-6.8.war
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: tomcat9

/var/lib/tomcat9/webapps/flowable-ui/WEB-INF/classes/flowable-default.properties:
  file.managed:
    - source: salt://workflow/flowable-default.properties.modeler
    - user: tomcat
    - group: tomcat
    - mode: 644
    - template: jinja
    - context:
      domain: {{ domain }}
      settings: {{ settings }}
    - require:
      - pkg: tomcat9

# old war files
/var/lib/tomcat9/webapps/flowable-idm.war:
  file.absent
/var/lib/tomcat9/webapps/flowable-idm:
  file.absent
/var/lib/tomcat9/webapps/flowable-modeler.war:
  file.absent
/var/lib/tomcat9/webapps/flowable-modeler:
  file.absent

{% endif %} {# osmajorrelease < 22 #}
{% endif %} {# 'workflow_tools' from pillar #}
{% endif %} {# 'workflow' from settings #}
{% endfor %}
{% endif %} {# 'workflow' from pillar #}
