{% set server_meta = pillar.get('server_meta', None) %}
{% if server_meta %}
{% set vpn = server_meta.get('vpn', None) %}
{% if vpn %}
{# install wireguard and resolvconf #}

{% if grains['osmajorrelease'] < 24 %}
resolvconf:
  pkg.installed
{% endif %}

wireguard:
  pkg.installed

/etc/wireguard:
  file.directory:
    - user: root
    - group: root
    - mode: 700
    - makedirs: False

{% set wireguard_client = pillar.get('wireguard_client', None) %}

{% if wireguard_client %}
{# Configure Client #}
{% set interface = wireguard_client.get('interface', None) %}
{% set server_host = wireguard_client.get('server_host', None) %}
{% set server_port = wireguard_client.get('server_port', None) %}
{% set server_pubkey = wireguard_client.get('server_pubkey', None) %}
{% set dns_server = wireguard_client.get('dns_server', None) %}
{% set client_details = server_meta.get('vpn', None ) %}
{% set ip = client_details.get('ip', None) %}
{% set allowed_ips = client_details.get('allowed-ips', None) %}
{% set privkey = client_details.get('privkey', None) %}

{% if interface and server_host and server_port and server_pubkey and ip and allowed_ips and privkey %}
/etc/wireguard/{{ interface }}.conf:
  file.managed:
    - source: salt://vpn/wg-client.conf
    - user: root
    - group: root
    - mode: 600
    - template: jinja
    - context:
      server_host: {{ server_host }}
      server_port: {{ server_port }}
      server_pubkey: {{ server_pubkey }}
      ip: {{ ip }}
      dns_server: {{ dns_server }}
      allowed_ips: {{ allowed_ips }}
      privkey: {{ privkey }}
    - require:
      - pkg: wireguard

wg-quick@{{ interface }}:
  service.running:
    - enable: True
    - require:
      - pkg: wireguard
    - watch:
      - file: /etc/wireguard/{{ interface }}.conf

{% else -%}
{% do salt.log.error("Wireguard Client - configuration is incomplete") -%}
failure:
  test.fail_without_changes:
    - name: "Wireguard Client configuration is incomplete, check your wireguard_client or server_meta configuration"
    - failhard: True
{% endif %} {# if interface and server_host and server_port and server_pubkey and ip and allowed_ips and privkey #}

{% else %}
{# Configure Server #}

{% set interface = vpn.get('interface', None) %}
{% set disabled = vpn.get('disabled', False) %}
{% set conf = vpn.get('conf', {}) %}
{% set peers = vpn.get('peers', []) %}
{% if interface and conf|length > 0 and peers|length > 0 %}
/etc/wireguard/{{ interface }}.conf:
  file.managed:
    - source: salt://vpn/wg-server.conf
    - user: root
    - group: root
    - mode: 600
    - template: jinja
    - context:
      conf: {{ conf }}
      peers: {{ peers }}
    - require:
      - pkg: wireguard

wg-quick@{{ interface }}:
  service.running:
    - enable: True
    - require:
      - pkg: wireguard
    - watch:
      - file: /etc/wireguard/{{ interface }}.conf
{% else %}
{% do salt.log.error("Wireguard server - configuration is incomplete") -%}
failure:
  test.fail_without_changes:
    - name: "Wireguard server - configuration is incomplete"
   - failhard: True
{% endif %} {# if interface and conf|length > 0 and peers|length > 0 #}

{% endif %} {# if client else server #}
{% endif %} {# if vpn #}
{% endif %} {# if server_meta #}
