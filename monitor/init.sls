{# Is this the monitoring server?  If so, install Grafana and Loki #}
{% set grafana = pillar.get('grafana', None) %}
{% set loki = pillar.get('loki', None) %}
{% set nginx = pillar.get('nginx', None) -%}
{% set promtail = pillar.get('promtail', None) %}
{% set sites = pillar.get('sites', {}) %}
{% if grafana %}

apt-transport-https:
  pkg.installed

software-properties-common:
  pkg.installed

grafana_repo_key:
  file.managed:
    - name: /root/grafana_repo.key
    - name: /root/repo/temp/grafana_repo.key
    - source: salt://monitor/gpg-key-grafana.txt
  cmd.run:
    - name: cat /root/repo/temp/grafana_repo.key | apt-key add -
    - require:
      - file: grafana_repo_key
      - pkg: apt-transport-https
      - pkg: software-properties-common

grafana_repo:
  file.managed:
    - name: /etc/apt/sources.list.d/grafana.list
    - require:
      - cmd: grafana_repo_key
    - contents: deb [signed-by=/etc/apt/keyrings/grafana.key arch=amd64] https://apt.grafana.com stable main

grafana_soft:
  pkg.installed:
    - name: grafana
    - refresh: True
    - require:
      - file: grafana_repo

/etc/grafana/grafana.ini:
  file:
    - managed
    - source: salt://monitor/grafana.ini
    - template: jinja
    - user: root
    - group: grafana
    - mode: 640
    - require:
      - pkg: grafana_soft

grafana-server.service:
  service.running:
    - enable: True
    - watch:
      - file: /etc/grafana/grafana.ini

{% endif %} {# grafana #}

{% if loki or promtail %}

monitor-group:
  group.present:
    - name: monitor
    - gid: 7499
    - system: True

monitor:
  user.present:
    - fullname: Monitoring
    - uid: 7499
    - groups:
      - adm
      - monitor
    - shell: /bin/bash
    - require:
      - group: monitor-group

/home/monitor/bin:
  file.directory:
    - user: monitor
    - group: monitor
    - mode: 755
    - makedirs: False
    - require:
      - user: monitor

{% endif %} {# loki or promtail #}

{% if loki %}

/home/monitor/bin/loki-linux-amd64:
  file:
    - managed
    - source: salt://monitor/loki-linux-amd64
    - user: monitor
    - group: monitor
    - mode: 755
    - require:
      - file: /home/monitor/bin

/home/monitor/bin/loki-local-config.yaml:
  file:
    - managed
    - source: salt://monitor/loki-local-config.yaml
    - user: monitor
    - group: monitor
    - mode: 644
    - require:
      - file: /home/monitor/bin

/home/monitor/bin/loki.service:
  file:
    - managed
    - source: salt://monitor/loki.service

/etc/systemd/system/loki.service:
  file.symlink:
    - target: /home/monitor/bin/loki.service
    - require:
      - file: /home/monitor/bin/loki.service

loki.service:
  service.running:
    - enable: True

{% endif %} {# loki #}

{% if promtail %}

/home/monitor/bin/promtail-linux-amd64:
  file:
    - managed
    - source: salt://monitor/promtail-linux-amd64
    - user: monitor
    - group: monitor
    - mode: 755
    - require:
      - file: /home/monitor/bin

/home/monitor/bin/promtail-local-config.yaml:
  file:
    - managed
    - source: salt://monitor/promtail-local-config.yaml
    - user: monitor
    - group: monitor
    - mode: 644
    - template: jinja
    - context:
      nginx: {{ nginx }}
      promtail: {{ promtail }}
      sites: {{ sites }}
    - require:
      - file: /home/monitor/bin

/home/monitor/bin/promtail.service:
  file:
    - managed
    - source: salt://monitor/promtail.service

/etc/systemd/system/promtail.service:
  file.symlink:
    - target: /home/monitor/bin/promtail.service
    - require:
      - file: /home/monitor/bin/promtail.service

promtail.service:
  service.running:
    - enable: True
    - watch:
      - file: /home/monitor/bin/promtail-local-config.yaml

{% endif %} {# promtail #}
